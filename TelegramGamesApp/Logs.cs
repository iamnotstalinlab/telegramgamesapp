﻿using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace TelegramGamesApp {
    public class Logs {
        public Logs() {}

        //Делаем записи в журнал
        public void WriteLog(string text) {

            using(FileStream fstream = new FileStream("./Logs", FileMode.OpenOrCreate)) {
                byte[] buffer = Encoding.Default.GetBytes("\n" + text);    // преобразуем строку в байты
                fstream.Seek(0, SeekOrigin.End);
                fstream.Write(buffer, 0, buffer.Length); // запись массива байтов в файл
            }
        }
    }
}
