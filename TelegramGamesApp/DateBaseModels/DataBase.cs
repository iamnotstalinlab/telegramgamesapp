﻿using System.Data.SQLite;

namespace TelegramGamesApp.DateBaseModels {
    public class DataBase :Logs {

        protected SQLiteConnection connection = new SQLiteConnection("Data Source=database.sqlite3");
        protected SQLiteDataReader dataReader;

        protected object dbLocker = new object();

        //Создаём объект
        public DataBase() {
            lock(dbLocker) {
                if(!System.IO.File.Exists("./database.sqlite3")) {
                    SQLiteConnection.CreateFile("database.sqlite3");
                }    
            }
        }

        //Открываем базу данных
        protected void OpenDatabase() {
            if(connection != null
                && connection.State == System.Data.ConnectionState.Closed) {
                connection.Open();
            }
        }

        //Закрываем базу данных
        protected void CloseDatabase() {
            if(connection != null
                && connection.State == System.Data.ConnectionState.Open) {
                connection.Close();
            }
        }

        

        

        

        

        

        



        

        

        

        

        

        

        

        

        

        

    }
}
