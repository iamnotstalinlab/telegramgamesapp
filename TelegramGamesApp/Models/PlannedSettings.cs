﻿// Модуль настроек планируемой игры. Здесь настраивается и планируется игра.
//

using System;
using System.Data.SQLite;

using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.ReplyMarkups;


namespace TelegramGamesApp.Models {
    public class PlannedSettings :PlannedGame {

        #region CREATE SECTION

        public EventHandler OnGameSettingsChanged;  //Событие при изменении игровых настроек

        //Создаём экземпляр настроек
        public PlannedSettings(string botToken) :base(botToken) {
            OnPlannedGameCreated += PlannedSettings_OnPlannedGameCreated;

            OnActivateGame += PlannedSettings_OnActivateGame;
            OnFinishGame += PlannedSettings_OnFinishGame;
            OnEnableGame += PlannedSettings_OnEnableGame;
            OnDisableGame += PlannedSettings_OnDisableGame;
        }

        //Событие, когда игра создаётся
        private void PlannedSettings_OnPlannedGameCreated(object sender, EventArgs e) {
            ShowActualGamePage(idCurrentUser);
        }

        //Событие когда текущая игра завершается
        private void PlannedSettings_OnFinishGame(object sender, EventArgs e) {

            foreach(int idHelper in authors.Keys) {
                if(!IsEnableGame) {
                    ShowQuickGamePage(idHelper);
                } else {
                    ShowPlannedGamePage(idHelper);
                }
            }
        }

        //Событие когда текущая игра запускается
        private void PlannedSettings_OnActivateGame(object sender, EventArgs e) {

            foreach(int idHelper in authors.Keys) {
                if(!IsEnableGame) {
                    ShowQuickGamePage(idHelper);
                } else {
                    ShowPlannedGamePage(idHelper);
                }
            }
        }

        //Событие когда игра отключается
        private void PlannedSettings_OnDisableGame(object sender, EventArgs e) {
            ShowPlannedGamePage(idCurrentUser);
        }

        //Событие когда игра включается
        private void PlannedSettings_OnEnableGame(object sender, EventArgs e) {    
            ShowPlannedGamePage(idCurrentUser);
        }





        //Создаём настройки планируемой игры
        public void Create(string botToken) {
            if(IdEditor != 0) {
                ChatMember chatMember =bot.GetChatMemberAsync(IdEditor, IdEditor).Result;
                authors.Add(IdEditor, new Author(chatMember.User));
            }
        }


        //Событие, когда изменяются игровые настройки
        private void Settings_OnSystemSettingsChanged(object sender, EventArgs e) {
            UpdateGameData();
            OnGameSettingsChanged?.Invoke(this, new EventArgs());
            ShowActualGamePage(idCurrentUser);
        }

        //Принимаем и обрабатываем сообщение
        public void AcceptMessage(Message message) {

            long idUser = message.From.Id;
            if (IsSystemEmpty) {
                AcceptSystemMessage(message);
            } else {
                AddAuthor(message.From);
                if (CanManage(message.From.Id)) {
                    if (!string.IsNullOrEmpty(authors[idUser].SetCallback)) {
                        AcceptSetMessage(message);
                    } else {
                        authors[idUser].IdEditMessage = 0;
                        ShowActualGamePage(idUser);
                    }
                }
            }
        }



        //Принимаем обратный вызов и обрабатываем его
        public void AcceptCallback(CallbackQuery e) {

            CallbackQuery callback = e;
            long idUser = callback.From.Id;

            if (IsSystemEmpty) {
                AcceptSystemCallback(callback);
            } else {
                AddAuthor(callback.From);
                if (CanManage(callback.From.Id)) {
                    if (callback.Message.MessageId == authors[idUser].IdEditMessage) {
                        AcceptPlannedSettingsCallback(callback);
                    } else {
                        bot.AnswerCallbackQueryAsync(callback.Id, "⚠️  Данное меню устарело. Я уже выслал вам новое.", false);
                        ShowActualGamePage(idUser);
                    }
                }
                
            }
        }
        

       

       

        #endregion

        #region ACCEPT PLANNED SETTINGS CALLBACK

        //Принимаем и обрабатываем обратный вызов планируемых настроек
        private void AcceptPlannedSettingsCallback(CallbackQuery callback) {

            long idUser = callback.From.Id;
            authors[idUser].IdEditMessage = callback.Message.MessageId;
            authors[idUser].SetCallback = null;

            switch(callback.Data) {

                case "ShowQuickGamePageCallback":
                    ShowQuickGamePage(idUser);
                    break;

                case "LaunchOrFinishQuickGameCallback":
                    LaunchOrFinishQuickGameThrowPlannedGame(idUser);
                    break;

                case "ShowPlannedGamePageCallback":
                    ShowPlannedGamePage(idUser);
                    break;

                case "EnableOrDisablePlannedGameCallback":
                    PlannedGameEnableOrDisable(callback);
                    break;

                default:
                    AcceptShowTunningPagesCallback(callback);
                    break;
            } 
        }

        //Принимаем установочный обратный вызов (с конкретной целью)
        private void AcceptSetCallback(CallbackQuery callback) {
            long idUser = callback.From.Id;
            authors[idUser].SetCallback = callback.Data;

            switch(authors[idUser].SetCallback) {
                case "SetStartTimeCallback":
                    ChangeGameTimePage(idUser);
                    break;

                case "SetEndTimeCallback":
                    ChangeGameTimePage(idUser);
                    break;

                case "SetStartChancesCallback":
                    ChangeProcessPage(idUser);
                    break;
                case "SetMaxWinnersCallback":
                    ChangeProcessPage(idUser);
                    break;

                case "SetStartTextCallback":
                    ChangeTextsPage(idUser);
                    break;

                case "SetEndTextCallback":
                    ChangeTextsPage(idUser);
                    break;
                default:
                    AcceptSystemCallback(callback);
                    break;
            }
        }

        //Принимаем обратный вызов страниц настроек
        private void AcceptShowTunningPagesCallback(CallbackQuery callback) {

            long idUser = callback.From.Id;
            if(!IsActiveGame) {
                if(!callback.Data.Contains("Set")) {
                    switch(callback.Data) {
                        case "ShowMainTunningPageCallback":
                            ShowMainTunningPage(idUser);
                            break;

                        case "ShowTimePageCallback":
                            ShowTimePage(idUser);
                            break;

                        case "ShowProcessPageCallback":
                            ShowProcessPage(idUser);
                            break;

                        case "ShowTextsPageCallback":
                            ShowTextsPage(idUser);
                            break;

                        default:
                            if(idUser == AdministratorId
                                || AllowControl) {
                                AcceptSystemCallback(callback);
                            } 
                            break;
                    }
                } else {
                    AcceptSetCallback(callback);
                }
            } else {
                string warning= "⚠️  Игра уже запущена. Изменим настройки позже." +
                    "Дождитесь заврешения или остановите игру самостоятельно.";
                bot.AnswerCallbackQueryAsync(callback.Id, warning, false);

                ShowPlannedGamePage(idUser);
            }
        }

        #endregion

        #region SET SETTINGS

        //Принимаем сообщение для установки планируемых настроек
        private void AcceptSetMessage(Message message) {

            long idUser = message.From.Id;
            string text = message.Text;
            switch(authors[idUser].SetCallback) {

                case "SetStartTimeCallback":
                    SetTime(idUser, text);
                    break;

                case "SetEndTimeCallback":
                    SetTime(idUser, text);
                    break;

                case "SetStartChancesCallback":
                    SetStartChancesOrMaxWinners(idUser, text);
                    break;

                case "SetMaxWinnersCallback":
                    SetStartChancesOrMaxWinners(idUser, text);
                    break;

                case "SetStartTextCallback":
                    SetStartOrEndTexts(idUser, text);
                    break;
                case "SetEndTextCallback":
                    SetStartOrEndTexts(idUser, text);
                    break;
                default:
                    AcceptSystemMessage(message);
                    break;
            }   
        }

        //Присылаем страницу изменения периода игрового времени
        private void ChangeGameTimePage(long idUser) {
            if(authors[idUser].SetCallback == "SetStartTimeCallback") {
                text = "Хорошо.\n" +
                    "Пришлите время <b>начала</b> турнира." +
                    "\n\n<i>Пример:</i> <b>08:35</b>";
            }

            if(authors[idUser].SetCallback == "SetEndTimeCallback") {
                text = "Хорошо.\n" +
                    "Пришлите время <b>окончания</b> турнира." +
                    "\n\n<i>Пример:</i> <b>09:35</b>";
            }
            keyboard = new InlineKeyboardMarkup(new[]{
                InlineKeyboardButton.WithCallbackData("Отмена", "ShowTimePageCallback")
            });
            EditOrSendPage(idUser, text);
        }

        //Устанавливаем игровое время
        private void SetTime(long idUser, string text) {

            DateTime time;
            if(text.Length == 5 &&
                DateTime.TryParse(text, out time)) {

                IdEditor = idUser;
                switch(authors[idUser].SetCallback) {
                    case "SetStartTimeCallback":
                        StartTime = time;
                        SaveStartTime();
                        break;

                    case "SetEndTimeCallback":
                        EndTime = time;
                        SaveEndTime();
                        break;
                }

                authors[idUser].IdEditMessage = 0;
                authors[idUser].SetCallback = null;

                UpdateGameData();
                OnGameSettingsChanged?.Invoke(this, new EventArgs());
                ShowTimePage(idUser);

            } else {
                this.text = "<i>Стоп.</i>" +
                    "\nНе могу понять формат времени." +
                    "\n\n<i>Пришлите ещё раз.</i>" +
                    "\n<i>Пример:</i> <b>20:35</b>";
                keyboard = new InlineKeyboardMarkup(new[]{
                    InlineKeyboardButton.WithCallbackData("Отмена", "ShowTimePageCallback")
                });
                SendSettingsMessage(idUser, this.text, keyboard);
            }
        }

        //Присылаем страницу измениния игрового процесса
        private void ChangeProcessPage(long idUser) {
            if(authors[idUser].SetCallback == "SetStartChancesCallback") {
                text = "Хорошо.\n" +
                    "Пришлите число шансов для каждого игрока." +
                    "\n\n<i>Пример:</i> <b>5</b>";
            }
            if(authors[idUser].SetCallback == "SetMaxWinnersCallback") {
                text = "Хорошо.\n" +
                    "Пришлите максимальное число победителей в игре." +
                    "\n\n<i>Пример:</i> <b>1</b>";
            }

            keyboard = new InlineKeyboardMarkup(new[]{
                InlineKeyboardButton.WithCallbackData("Отмена", "ShowProcessPageCallback")
            });
            EditOrSendPage(idUser, text);
        }

        //Устанавливаем максимальное число ходов или победителей
        private void SetStartChancesOrMaxWinners(long idUser, string text) {
            int num;
            if(int.TryParse(text, out num) && num >0) {

                IdEditor = idUser;
                switch(authors[idUser].SetCallback) {
                    case "SetStartChancesCallback":
                        StartChances = num;
                        SaveStartChances();
                        break;

                    case "SetMaxWinnersCallback":
                        MaxWinners = num;
                        SaveMaxWinners();
                        break;
                }

                authors[idUser].IdEditMessage = 0;
                authors[idUser].SetCallback = null;

                UpdateGameData();
                OnGameSettingsChanged?.Invoke(this, new EventArgs());
                ShowProcessPage(idUser);
            } else {
                this.text = "<i>Стоп.</i>" +
                    "\nНе могу понять формат числа. Пришлите по примеру" +
                    "\n\n<i>Пример:</i> <b>1</b>";
                keyboard = new InlineKeyboardMarkup(new[]{
                    InlineKeyboardButton.WithCallbackData("Отмена", "ShowProcessPageCallback")
                });
                SendSettingsMessage(idUser, this.text, keyboard);
            }
        }

        //Присылаем страницу изменения начального и конечного текста
        private void ChangeTextsPage(long idUser) {

            switch(authors[idUser].SetCallback) {
                case "SetStartTextCallback":
                    text = "Хорошо.\n" +
                        "Теперь, пришлите текст <b>в объявлении</b> каждой игры." +
                        "\n\n<i>Пример:</i>" +
                        "\nЭто текст в объявлении игры.";
                    break;
                case "SetEndTextCallback":
                    text = "Хорошо.\n" +
                        "Теперь, пришлите текст <b>в тирах</b> каждой игры." +
                        "\n\n<i>Пример:</i>" +
                        "\nЭто текст в титрах игры.";
                    break;
            }

            keyboard = new InlineKeyboardMarkup(new[]{
                InlineKeyboardButton.WithCallbackData("Отмена", "ShowTextsPageCallback")
            });
            EditOrSendPage(idUser, text);
        }

        //Устанавливаем начальный и конечный текст
        private void SetStartOrEndTexts(long idUser, string text) {

            IdEditor = idUser;
            switch(authors[idUser].SetCallback) {

                case "SetStartTextCallback":
                    StartText = text;
                    SaveStartText();
                    break;

                case "SetEndTextCallback":
                    EndText = text;
                    SaveEndText();
                    break;
            }

            authors[idUser].IdEditMessage = 0;
            authors[idUser].SetCallback = null;

            UpdateGameData();
            OnGameSettingsChanged?.Invoke(this, new EventArgs());
            ShowTextsPage(idUser);
        }

        #endregion

        #region GAME ACTIONS AND PAGES

        //Показываем действующую страницу
        private void ShowActualGamePage(long idUser) {
            if(IsActiveGame
                && !IsEnableGame) {
               
                ShowQuickGamePage(idUser);
            } else {
                ShowPlannedGamePage(idUser);
            }
        }
 
        //Включаем или выключаем обычную игру
        private void PlannedGameEnableOrDisable(CallbackQuery callback) {
            if(!IsActiveGame) {
                if(IsEnableGame) {
                    DisableGame(true);
                } else {
                    EnableGame(true);
                }

                IdEditor = callback.From.Id;
            } else {
                string warning = "⚠️  Запущена скорая игра. Запланируем турнир после её завершения.";
                bot.AnswerCallbackQueryAsync(callback.Id, warning, false);
            }
        }
        
        //Показываем страницу планируемой игры
        private void ShowPlannedGamePage(long idUser) {

            string status = string.Empty;
            string buttonText = status;
            text = "<i>Плановая игра</i>\n";

            if(IsEnableGame) {
                status = "Состояние: ";
                if(IsActiveGame) {
                    status +="🚜 <b>Выполняется</b>";
                } else {
                    status += "⛽️  <b>Ожидает</b>";
                }

                buttonText = "Отключить";
            } else {
                status = "Состояние: 🔌 <b>Отключена</b>";
                buttonText = "Включить";
            }

            text += status;
            text += $"\n\n<b>Пояснения:</b>" +
                $"\n⏰  Игровое время с <b>{StartTime.ToString("HH:mm")}</b> " +
                $"по <b>{EndTime.ToString("HH:mm")}</b>" +
                $"\n🍀  У каждого игрока по <b>{StartChances}</b> шансов (а/ов). " +
                $"\n🏆  В игре <b>{MaxWinners}</b> чемпион(а/ов). " +
                $"\n\n📣  {StartText}" +
                $"\n🎉  {EndText}";
            if(authors.ContainsKey(IdEditor)) {
                text += $"\n\n💡  Ответственный: <a href=\"tg://user?id={IdEditor}\">ID{authors[IdEditor].Id}</a>";
            }

            keyboard = new InlineKeyboardMarkup(new[]{
                new []{
                    InlineKeyboardButton.WithCallbackData(buttonText, "EnableOrDisablePlannedGameCallback")
                },

                new []{
                    InlineKeyboardButton.WithCallbackData("Скорая игра", "ShowQuickGamePageCallback"),
                    InlineKeyboardButton.WithCallbackData("Настройки", "ShowMainTunningPageCallback")
                }
            });
            EditOrSendPage(idUser, text);
        }

        #endregion

        #region SHOW TUNNING PAGES

        //Показываем основную страницу настроек
        private void ShowMainTunningPage(long idUser) {

            string status = string.Empty;
            string buttonText = status;
            text = "<i>Настройки игр</i>";

            text += $"\n\n<b>Настройки текущей игры:</b>" +
                $"\n⏰  Игровое время с <b>{StartTime.ToString("HH:mm")}</b> " +
                $"по <b>{EndTime.ToString("HH:mm")}</b>" +
                $"\n🍀  У каждого игрока по <b>{StartChances}</b> шанс (а/ов). " +
                $"\n🏆  В игре <b>{MaxWinners}</b> чемпион(а/ов). " +
                $"\n\n📣  {StartText}" +
                $"\n🎉  {EndText}" +
                $"\n\n<i>Что следует изменить?</i>";

            keyboard = new InlineKeyboardMarkup(new[]{
                new []{
                    InlineKeyboardButton.WithCallbackData("Время", "ShowTimePageCallback"),
                    InlineKeyboardButton.WithCallbackData("Процесс", "ShowProcessPageCallback")
                },
                new []{
                    InlineKeyboardButton.WithCallbackData("Объявление и титры", "ShowTextsPageCallback")
                },
                new []{
                    InlineKeyboardButton.WithCallbackData("На главную", "ShowPlannedGamePageCallback"),
                    InlineKeyboardButton.WithCallbackData("Система", "ShowSystemPageCallback")
                }
            });

            EditOrSendPage(idUser, text);
        }

        //Показываем страницу настроек времени
        private void ShowTimePage(long idUser) {
            string status = string.Empty;
            string buttonText = status;
            text = "<i>Настроить время игры</i>";

            text += $"\n\n<b>Настройки текущей игры:</b>" +
                $"\n⏰  Начинаем в <b>{StartTime.ToString("HH:mm")}</b> " +
                $"\n🕰  Заканчиваем в <b>{EndTime.ToString("HH:mm")}</b>" +
                $"\n\n<i>Что следует изменить?</i>";

            keyboard = new InlineKeyboardMarkup(new[]{
                new []{
                    InlineKeyboardButton.WithCallbackData("Начало", "SetStartTimeCallback"),
                    InlineKeyboardButton.WithCallbackData("Конец", "SetEndTimeCallback")
                },
                new []{
                    InlineKeyboardButton.WithCallbackData("Назад", "ShowMainTunningPageCallback")
                }
            });
            EditOrSendPage(idUser, text);
        }

        //Показываем страницу настроек игрового процесса
        private void ShowProcessPage(long idUser) {
            string status = string.Empty;
            text = "<i>Настроить игровой процесс</i>";


            text += $"\n\n<b>Настройки текущей игры:</b>" +
                $"\n🍀  У каждого игрока по <b>{StartChances}</b> шанс (а/ов). " +
                $"\n🏆  В игре <b>{MaxWinners}</b> чемпион(а/ов). " +
                $"\n\n<i>Что следует изменить?</i>";

            keyboard = new InlineKeyboardMarkup(new[]{
                new []{
                    InlineKeyboardButton.WithCallbackData("Шансы", "SetStartChancesCallback"),
                    InlineKeyboardButton.WithCallbackData("Чемпионов", "SetMaxWinnersCallback")
                },
                new []{
                    InlineKeyboardButton.WithCallbackData("Назад", "ShowMainTunningPageCallback")
                }
            });

            EditOrSendPage(idUser, text);
        }

        //Показываем страницу настроек текста
        private void ShowTextsPage(long idUser) {
            string status = string.Empty;
            text = "<i>Настроить текст в объявлении и титрах</i>";

            text += $"\n<b>Настройки текущей игры:</b>" +
                $"\n\n📣  Текст в объявлении:\n{StartText}" +
                $"\n\n🎉  Текст в титрах:\n{EndText}" +
                $"\n\n<i>Что следует изменить?</i>";

            keyboard = new InlineKeyboardMarkup(new[]{
                new []{
                    InlineKeyboardButton.WithCallbackData("Объявление", "SetStartTextCallback"),
                    InlineKeyboardButton.WithCallbackData("Титры", "SetEndTextCallback")
                },
                new []{
                    InlineKeyboardButton.WithCallbackData("Назад", "ShowMainTunningPageCallback")
                }
            });
            EditOrSendPage(idUser, text);
        }

        #endregion

        #region DATA BASE

        //Устанавливаем максимальное количество бросков в планируемой игре
        private void SaveStartChances() {

            lock(dbLocker) {
                OpenDatabase();

                string sCommand = "UPDATE PlannedGames SET StartChances =@startChances, IdPlannedEditor =@idPlannedEditor" +
                    " WHERE Id =1";

                SQLiteCommand command = new SQLiteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@startChances", StartChances);
                command.Parameters.AddWithValue("@idPlannedEditor", IdEditor);

                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }

        //Устанавливаем максимальное число победителей в планируемой игре
        private void SaveMaxWinners() {

            lock(dbLocker) {
                OpenDatabase();

                string sCommand = "UPDATE PlannedGames SET MaxWinners =@maxWinners, IdPlannedEditor =@idPlannedEditor" +
                    " WHERE Id =1";

                SQLiteCommand command = new SQLiteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@maxWinners", MaxWinners);
                command.Parameters.AddWithValue("@idPlannedEditor", IdEditor);

                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }

        //Устанавливаем текст в начале планируемой игры
        private void SaveStartText() {

            lock(dbLocker) {
                OpenDatabase();

                string sCommand = "UPDATE PlannedGames SET StartText =@startText, IdPlannedEditor =@idPlannedEditor WHERE Id =1";

                SQLiteCommand command = new SQLiteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@startText", StartText);
                command.Parameters.AddWithValue("@idPlannedEditor", IdEditor);

                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }

        //Устанавливаем текст в конце планируемой игры
        private void SaveEndText() {

            lock(dbLocker) {
                OpenDatabase();

                string sCommand = "UPDATE PlannedGames SET EndText =@endText, IdPlannedEditor =@idPlannedEditor WHERE Id =1";

                SQLiteCommand command = new SQLiteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@endText", EndText);
                command.Parameters.AddWithValue("@idPlannedEditor", IdEditor);

                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }
        #endregion



    }
}
