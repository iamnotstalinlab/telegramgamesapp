﻿// Базовый модуль системы Телеграм игр. Содержит все основные системные настройки.
//


//SetGameChatAddressCallback

using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramGamesApp.DateBaseModels;
namespace TelegramGamesApp.Models {

    //Системный класс
    public class System : DataBase{

        #region INITIALIZE
        public ITelegramBotClient bot;   //Объект бота-исполнителя

        protected Dictionary<long, Author> authors; //Словарь авторов обращения турнира
        protected long IdEditor; //Идентификатор последнего редактора настроек планируемой игры

        public long AdministratorId { get; protected set; }

        protected Chat GameChat { get; set; }
        public long GameChatId { get; protected set; }
        private string GameChatInviteLink { get; set; }

        public long GameChannelId { get; protected set; }
        private string GameChannelInviteLink { get; set; }


        public bool AllowControl { get; protected set; } //Разрешить администраторам основного чата упралять системой

        public bool IsSystemEmpty { get; protected set; }
        protected int idQuickEditor;   //идентификатор редактора настроек скорой игры

        public EventHandler OnSystemSettingsCreated;    //Событие создании системных настроек
        public EventHandler OnSystemSettingsChanged;    //Событие при изменении системных настроек

        //Создаём экземпляр системы
        public System(string botToken) {
            bot = new TelegramBotClient(botToken);
            authors = new Dictionary<long, Author>();
            CreateTableSystems(connection);

            IsSystemEmpty = true;
            LoadSystemSettings();

            if(IdEditor != 0) {
                ChatMember chatMember = bot.GetChatMemberAsync(IdEditor, IdEditor).Result;
                authors.Add(IdEditor, new Author(chatMember.User));
            }
            
        }
        #endregion

        #region ACCEPT MESSAGES AND CALLBACKS

        //Принимаем системное сообщение
        protected void AcceptSystemMessage(Message message) {

            long idUser = message.From.Id;
            AddAuthor(message.From);

            if(IsSystemEmpty) {
                CreateSystemSettings(idUser, message.Text);
            } else {
                if(!string.IsNullOrEmpty(authors[idUser].SetCallback)) {
                    AcceptSetMessage(message);
                }
            }

        }

        //Принимаем сообщение с параметром для установки
        private void AcceptSetMessage(Message message) {
            long idUser = message.From.Id;
            string command = message.Text;

            switch(authors[idUser].SetCallback) {
                case "SetGameChatIdCallback":
                    SetChatOrChannelIds(idUser, command);
                break;

                case "SetGameChannelIdCallback":
                    SetChatOrChannelIds(idUser, command);
                break;
            }
        }

        //Принимаем системный вызов
        protected void AcceptSystemCallback(CallbackQuery callback) {
            long idUser = callback.From.Id;

            if(IsSystemEmpty) {
                AcceptSystemCreateCallback(callback);
            } else {
                switch(callback.Data) {

                    case "ShowSystemPageCallback":
                        ShowSystemPage(idUser);
                        break;

                    case "SetGameChatIdCallback":
                        authors[idUser].SetCallback = callback.Data;
                        ShowAddressesPage(idUser);
                        break;

                    case "SetGameChannelIdCallback":
                        authors[idUser].SetCallback = callback.Data;
                        ShowAddressesPage(idUser);
                        break;

                    case "SetAllowControlCallback":
                        SetAllowControl(idUser, callback.Id);
                        break;
                }
            }

        }

        //Принимаем обратный вызов для создания системных настроек
        private void AcceptSystemCreateCallback(CallbackQuery callback) {

            long idUser = callback.From.Id;
            switch(callback.Data) {
                case "YesIamFounderCallback":

                    AdministratorId = callback.From.Id;
                    text = "Замечательно. Рад с вами познакомиться." +
                        "\nДавайте продолжим настройку.";

                    SendSettingsMessage(idUser, text, null);
                    CreateSystemSettings(idUser, null);
                    break;
                case "NoIamnotFounderCallback":

                    text = "Тогда приглашаю основателя." +
                        "\nПусть напишет и мы проведём настройку.";
                    
                    AdministratorId = 0;
                    SendCancelMessage(idUser, text);
                    break;

                default:
                    CreateAndSaveSystemSettings(callback);
                    break;
            }
        }



        //Проверяем имеет ли пользователь права управлять турниром. 
        protected bool CanManage(long idUser) {

            if (idUser == AdministratorId) {
                return true;
            } else {

                //Сделал этот блок, чтобы ускорить обработку. Если участника нет среди администраторов чата, то выходим из функции
                if (AllowControl) {
                    try {
                        List<ChatMember> admins = bot.GetChatAdministratorsAsync(GameChannelId).Result.ToList();
                        ChatMember tMember = admins.Find(item => item.User.Id == idUser);
                        if (tMember == null) {
                            return false;
                        } else {
                            return true;
                        }
                    } catch {
                        return false;
                    }
                } else {
                    return false;
                }
            }

            
        }

        //Добавляем автора в словарь 
        protected void AddAuthor(User user) {
            if (!authors.ContainsKey(user.Id)) {
                authors.Add(user.Id, new Author(user)); ;
            }
        }

        #endregion

        #region SETTERS
        //Устанавливаем адреса чата или канала
        //Устанавливаем уникальный номер администратора системы
        protected string text = string.Empty;
        protected InlineKeyboardMarkup keyboard = null;

        //Устанавливаем адреса чата или канала
        private void SetChatOrChannelIds(long idUser, string command) {

            string tCallback = authors[idUser].SetCallback;

            string type = string.Empty;
            if(tCallback == "SetGameChatIdCallback") {
                type = "чата";
            } else {
                type = "канала";
            }

            text = "<i>Стоп.</i> Не получается." +
                $"\nДавайте проверим адрес <b>{type}</b> и повторим." +
                "\n\n<i>Пример:</i> <b>@wearegames</b>";

            keyboard = new InlineKeyboardMarkup(new[]{
                 InlineKeyboardButton.WithCallbackData("Отмена", "ShowSystemPageCallback")
                });

                Task.Run(() => {
                    try {
                        if(command.Contains("@")) {
                            Chat chat = bot.GetChatAsync(command).Result;
                            if((tCallback == "SetGameChatIdCallback"
                                && (chat.Type == ChatType.Supergroup))
                                || (tCallback == "SetGameChannelIdCallback")
                                && (chat.Type == ChatType.Channel)) {

                                IdEditor = idUser;

                                if(tCallback == "SetGameChatIdCallback") {

                                    GameChat = chat;
                                    GameChatId = chat.Id;
                                    GameChatInviteLink = chat.InviteLink;

                                    SaveGameChatId();
                                } else {

                                    GameChannelId = chat.Id;
                                    GameChannelInviteLink = chat.InviteLink;

                                    SaveGameChannelId();
                                }

                                authors[idUser].IdEditMessage = 0;
                                authors[idUser].SetCallback = null;

                                OnSystemSettingsChanged?.Invoke(this, new EventArgs());
                                ShowSystemPage(idUser);
                            } else {
                                SendSettingsMessage(idUser, this.text, keyboard);
                            }
                        } else {
                            SendSettingsMessage(idUser, this.text, keyboard);
                        }
                    } catch {
                        SendSettingsMessage(idUser, this.text, keyboard);
                    }
                });
            
        }

        //Меняем права управления системой
        private void SetAllowControl(long idUser, string idCallback) {

            string notify = "⚠️  Только основатель настраивает управление.";
            if(AdministratorId == idUser) {
                if(AllowControl == false) {
                    AllowControl = true;
                } else {
                    AllowControl = false;
                }

                IdEditor = idUser;
                authors[idUser].SetCallback = null;
                
                SaveAllowControl();
                ShowSystemPage(idUser);

                notify = "⚠️  Вы успешно обновили настройки управления.";
            }
            
            bot.AnswerCallbackQueryAsync(idCallback, notify, false);

        }

        #endregion

        #region SHOW PAGES

        //Присылаем страницу для изменения адресов
        private void ShowAddressesPage(long idUser) {
            if(authors[idUser].SetCallback == "SetGameChatIdCallback") {
                text = "Хорошо.\n" +
                    "Теперь, пришлите адрес <b>чата</b>." +
                    "\n\n<i>Пример:</i>" +
                    "\n<b>@wearegames</b>";
            }
            if(authors[idUser].SetCallback == "SetGameChannelIdCallback") {
                text = "Хорошо.\n" +
                    "Теперь, пришлите адрес <b>канала</b>." +
                    "\n\n<i>Пример:</i> <b>@wearegames</b>";
            }

            keyboard = new InlineKeyboardMarkup(new[]{
                InlineKeyboardButton.WithCallbackData("Отмена", "ShowSystemPageCallback")
            });

            EditUserMessage(idUser, text);
        }

        //Показываем страницу системных настроек
        private void ShowSystemPage(long idUser) {

            string sAllowControl = "";
            text = "<i>Системные настройки</i>";

            if(AllowControl) {
                sAllowControl = "🥁  Помогают администраторы канала.";
            } else {
                sAllowControl = "👑  Вы управляете системой.";
            }

            text = "<i>Настроить игровую систему</i>" +
                "\n\n<b>Текущие настройки системы:</b>" +
                $"\n🎡  Чат: <i><a href=\"{GameChatInviteLink}\">[{GameChatId}]</a></i>" +
                $"\n🛰  Канал: <i><a href=\"{GameChannelInviteLink}\">[{GameChannelId}]</a></i>" +
                $"\n{sAllowControl}" +
                $"\n\n<i>Что будем улучшать?</i>";

            keyboard = new InlineKeyboardMarkup(new[]{
                new []{
                    InlineKeyboardButton.WithCallbackData("Чат", "SetGameChatIdCallback"),
                    InlineKeyboardButton.WithCallbackData("Канал", "SetGameChannelIdCallback"),

                },
                new []{
                    InlineKeyboardButton.WithCallbackData("Назад", "ShowMainTunningPageCallback"),
                    InlineKeyboardButton.WithCallbackData("Управление", "SetAllowControlCallback"),
                }
            });


            EditOrSendPage(idUser, text);
        }

        #endregion

        #region CREATE SYSTEM SETTINGS

        //Создаём настройки системы
        private void CreateSystemSettings(long idUser, string mText) {
            if(AdministratorId == 0) {
                CreateAdministratorId(idUser);
            } else {

                if(AdministratorId == idUser) {
                    if(GameChatId ==0) {
                        CreateGameChatId(idUser, mText);
                    } else {
                        if(GameChannelId ==0) {
                            CreateGameChannelId(idUser, mText);
                        } else {
                            CreateAllowControl(idUser);
                        }
                    }
                }
            }

        }

        //Создаём администратора системы под уникальным номером
        private void CreateAdministratorId(long idUser) {
            text = "<i>🥇  Шаг <b>1</b>/4.</i> [Основная настройка]" +
                    "\nЗдравствуйте." +
                    "\nДавайте установим основные настройки игры." +
                    "\n\nВы ответственный руководитель?";

            keyboard = new InlineKeyboardMarkup(new[]{
                new []{
                    InlineKeyboardButton.WithCallbackData("Да, я руководитель", "YesIamFounderCallback")
                },

                new []{
                    InlineKeyboardButton.WithCallbackData("Нет, я только помогаю", "NoIamnotFounderCallback")
                }});
            SendSettingsMessage(idUser, text, keyboard);
        }

        //Создаём адрес чата
        private async void CreateGameChatId(long idUser, string mText) {

            keyboard = null;
            if(!string.IsNullOrEmpty(mText)) {
                text = "<i>🥇  Шаг <b>2</b>/4</i>. [Системная настройка]" +
                            "\nНе смог найти такую <b>супергруппу</b>." +
                            "\n\n<i>(Проверьте адрес и пришлите снова)</i>" +
                            "\n<i>Пример: @telegramgamechat</i>";

                if(mText.Contains("@")) {

                    await Task.Run(() => {
                        try {
                            Chat chat = bot.GetChatAsync(mText).Result;
                            if(chat.Type == ChatType.Supergroup) {

                                GameChat = chat;
                                GameChatId = chat.Id;
                                GameChatInviteLink = chat.InviteLink;

                                text = "Замечательно! С этим тоже справились." +
                                    "\nПродолжаем настройку.";

                                SendSettingsMessage(idUser, text, null);
                                CreateSystemSettings(idUser, null);
                            } else {
                                SendSettingsMessage(idUser, text, keyboard);
                            }

                        } catch {
                            SendSettingsMessage(idUser, text, keyboard);
                        }
                    });
                } else {
                    SendSettingsMessage(idUser, text, keyboard);
                }
            } else {
                text = "<i>🥇  Шаг <b>2</b>/4</i>. [Системная настройка]" +
                        "\nУстанавливаем <b>супергруппу</b>, для сохранения истории игр." +
                        "\n\n<i>(Пришлите адрес и сделайте меня администратором)</i>" +
                        "\n<i>Пример: @telegramgamechat</i>";

                SendSettingsMessage(idUser, text, null);
            }
        }

        //Создаём адрес игрового канала 
        private async void CreateGameChannelId(long idUser, string mText) {
            keyboard = null;

            if(!string.IsNullOrEmpty(mText)) {
                text = "<i>🥇  Шаг <b>3</b>/4.</i>. [Системная настройка]" +
                            "\nНе смог найти такой <b>канал</b>." +
                            "\n\n<i>(Проверьте адрес и пришлите снова)</i>" +
                            "\n<i>Пример: @telegramgamechat</i>";

                if(mText.Contains("@")) {

                    await Task.Run(() => {
                        try {
                            Chat channel = bot.GetChatAsync(mText).Result;
                            if(channel.Type == ChatType.Channel) {

                                GameChannelId = channel.Id;
                                GameChannelInviteLink = channel.InviteLink;

                                text = "Великолепно! Осталось совсем немножко." +
                                    "\nПродолжаем настройку.";
                                SendSettingsMessage(idUser, text, null);
                                CreateSystemSettings(idUser, null);
                            } else {
                                SendSettingsMessage(idUser, text, keyboard);
                            }

                        } catch {
                            SendSettingsMessage(idUser, text, keyboard);
                        }
                    });
                } else {
                    SendSettingsMessage(idUser, text, keyboard);
                }
            } else {
                text = "<i>🥇  Шаг <b>3</b>/4</i>. [Системная настройка]" +
                        "\nУстанавливаем <b>канал</b>, где проходят игры." +
                        "\n\n<i>(Пришлите адрес и сделайте меня администратором)</i>" +
                        "\n<i>Пример: @telegramgamechat</i>";

                SendSettingsMessage(idUser, text, null);
            }
        }

        //Создаём разрешение управлять игрой всем администраторам чата
        private void CreateAllowControl(long idUser) {
            text = "<i>🥇  Шаг <b>4</b>/4</i>. [Системная настройка]" +
                "\nУстанавливаем права для помощников." +
                "\n\nРазрешаем администраторам <b>общего совета</b> управлять системой?";

            keyboard = new InlineKeyboardMarkup(new[]{
                new []{
                    InlineKeyboardButton.WithCallbackData("Да, пусть тоже управляют", "AllowControlCallback")
                },
                new []{
                    InlineKeyboardButton.WithCallbackData("Нет, управляю только я", "DisallowControlCallback")
                }});
            SendSettingsMessage(idUser, text, keyboard);
        }

        //Сохраняем созданные системные настройки
        private void CreateAndSaveSystemSettings(CallbackQuery callback) {
            long idUser = callback.From.Id;
            if(callback.Data == "AllowControlCallback"
                    || callback.Data == "DisallowControlCallback") {

                
                switch(callback.Data) {
                    case "AllowControlCallback":
                        AllowControl = true;
                        break;
                    case "DisallowControlCallback":
                        AllowControl = false;
                        break;
                }

                //Создаём системную таблицу
                IdEditor = idUser;
                
                CreateTableSystems(connection);
                InitializeAndSaveSystemSettings();

                IsSystemEmpty = false;
                
                text = "Ура! Вот и всё! Мы - настроили игру." +
                    "\nТеперь можно запустить турнир.";
                SendSettingsMessage(idUser, text, null);

                authors[idUser].IdEditMessage = 0;
                OnSystemSettingsCreated?.Invoke(this, new EventArgs());
            }
        }

        //Отправляем или редактируем сообщение
        protected void EditOrSendPage(long idUser, string text) {
            if (authors.ContainsKey(idUser)) {
                if (authors[idUser].IdEditMessage != 0) {
                    EditUserMessage(idUser, text);
                } else {
                    SendSettingsMessage(idUser, text, keyboard);
                }
            } 
        }

        #endregion

        #region DATE BASE

        //Создаём системную таблицу
        private void CreateTableSystems(SQLiteConnection cnct) {
            lock(dbLocker) {
                
                OpenDatabase();
                string sCommand = @"CREATE TABLE IF NOT EXISTS Systems(
                        AdministratorId   INTEGER NOT NULL UNIQUE,
                        GameChatId   INTEGER NOT NULL,
                        GameChatInviteLink    TEXT NOT NULL,
	                    GameChannelId    INTEGER NOT NULL,
                        GameChannelInviteLink    TEXT NOT NULL,
                        AllowControl    TEXT NOT NULL,
                        IdEditor INTEGER NOT NULL);";

                SQLiteCommand command = new SQLiteCommand();

                command.Connection = cnct;
                command.CommandText = sCommand;
                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }

        //Сохраняем начальные настройки системы
        private void InitializeAndSaveSystemSettings() {

            lock(dbLocker) {
                OpenDatabase();

                string sCommand = "INSERT INTO Systems(" +
                    "AdministratorId, GameChatId, GameChatInviteLink, GameChannelId, GameChannelInviteLink" +
                    ", AllowControl, IdEditor) VALUES (@administratorId, @gameChatId" +
                    ", @gameChatInviteLink, @gameChannelId, @gameChannelInviteLink, @allowControl, @idEditor)";

                SQLiteCommand command = new SQLiteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;

                command.Parameters.AddWithValue("@administratorId", AdministratorId);
                command.Parameters.AddWithValue("@gameChatId", GameChatId);
                command.Parameters.AddWithValue("@gameChatInviteLink", GameChatInviteLink);
                command.Parameters.AddWithValue("@gameChannelId", GameChannelId);
                command.Parameters.AddWithValue("@gameChannelInviteLink", GameChannelInviteLink);
                command.Parameters.AddWithValue("@allowControl", AllowControl.ToString());
                command.Parameters.AddWithValue("@idEditor", IdEditor);

                command.ExecuteNonQuery();
                CloseDatabase();
            }
            SaveEditor();
        }

        //Загружаем системные настройки из базы данных
        private void LoadSystemSettings() {

            lock(dbLocker) {
                OpenDatabase();

                string sCommand = "SELECT * FROM Systems";

                SQLiteCommand command = new SQLiteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;

                dataReader = command.ExecuteReader();
                if(dataReader.HasRows) {

                    while(dataReader.Read()) {
                        AdministratorId = int.Parse(dataReader["AdministratorId"].ToString());

                        GameChatId = long.Parse(dataReader["GameChatId"].ToString());
                        GameChatInviteLink = dataReader["GameChatInviteLink"].ToString();

                        GameChannelId = long.Parse(dataReader["GameChannelId"].ToString());
                        GameChannelInviteLink = dataReader["GameChannelInviteLink"].ToString();

                        AllowControl = bool.Parse(dataReader["AllowControl"].ToString());
                        IdEditor = int.Parse(dataReader["IdEditor"].ToString());

                        IsSystemEmpty = false;
                    }
                }
                dataReader.Close();
                CloseDatabase();
            }
        }

        //Сохраняем адрес чата с историей игр
        private void SaveGameChatId() {

            lock(dbLocker) {
                OpenDatabase();
                string sCommand = "UPDATE Systems SET GameChatId =@gameChatId" +
                    ", GameChatInviteLink =@gameChatInviteLink, IdEditor =@idEditor ";

                SQLiteCommand command = new SQLiteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@gameChatId", GameChatId);
                command.Parameters.AddWithValue("@gameChatInviteLink", GameChatInviteLink);
                command.Parameters.AddWithValue("@idEditor", IdEditor);

                command.ExecuteNonQuery();
                CloseDatabase();
            }
            SaveEditor();
        }

        //Сохраняем адрес канала с игрой
        private void SaveGameChannelId() {

            lock(dbLocker) {
                OpenDatabase();
                string sCommand = "UPDATE Systems SET GameChannelId =@gameChannelId" +
                    ", GameChannelInviteLink =@gameChannelInviteLink, IdEditor =@idEditor";

                SQLiteCommand command = new SQLiteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@gameChannelId", GameChannelId);
                command.Parameters.AddWithValue("@gameChannelInviteLink", GameChannelInviteLink);
                command.Parameters.AddWithValue("@idEditor", IdEditor);

                command.ExecuteNonQuery();
                CloseDatabase();
            }
            SaveEditor();
        }

        //Сохраняем разрешения управлять игрой администраторам чата
        private void SaveAllowControl() {

            lock(dbLocker) {
                OpenDatabase();
                string sCommand = "UPDATE Systems SET AllowControl =@allowControl, IdEditor =@idEditor ";

                SQLiteCommand command = new SQLiteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@allowControl", AllowControl.ToString());
                command.Parameters.AddWithValue("@idEditor", IdEditor);

                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }

        //Сохраняем разрешения управлять игрой администраторам чата
        protected void SaveEditor() {

            lock(dbLocker) {
                OpenDatabase();
                string sCommand = "UPDATE Systems SET IdEditor =@idEditor ";

                SQLiteCommand command = new SQLiteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@idEditor", IdEditor);

                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }

        #endregion

        #region SEND AND EDIT MESSAGE

        //Отправляем сообщение текущему пользователю
        protected long idCurrentUser;  //Уникальный идентификатор последнего пользователя
        public void SendSettingsMessage(long idUser, string title, InlineKeyboardMarkup keyboard) {

            try {
                
                if(keyboard != null) {
                    authors[idUser].IdEditMessage = bot.SendTextMessageAsync(
                            chatId: idUser
                            , text: title
                            , replyMarkup: keyboard
                            , disableWebPagePreview: true
                            , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html).Result.MessageId;
                } else {
                    authors[idUser].IdEditMessage = bot.SendTextMessageAsync(chatId: idUser
                    , text: title
                    , disableWebPagePreview: true
                    , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html).Result.MessageId;
                }
                idCurrentUser = idUser;
            } catch { }
        }

        //Отправляем сообщение и обнуляем пользователя
        protected async void SendCancelMessage(long idUser, string text) {

            //Ставим await чтобы запустить в отдельном потоке, так как результат нас не итересует.
            //Всё равно idEditMessage ставим в нуль (такие операции, думаю, пройдут быстрее, чем одна с ожиданием)
            await bot.SendTextMessageAsync(chatId: idUser,
                text: text,
                parseMode: Telegram.Bot.Types.Enums.ParseMode.Html);

            authors[idUser].IdEditMessage = 0;
            idCurrentUser = idUser;
        }


        //Редактируем сообщение для пользователя
        protected async void EditUserMessage(long idUser, string text) {
            await Task.Run(() => {
                try {
                    bot.EditMessageTextAsync(idUser
                       , authors[idUser].IdEditMessage
                       , text: text
                       , replyMarkup: keyboard
                       , disableWebPagePreview: true
                       , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html);

                    idCurrentUser = idUser;
                } catch { }
            });
        }

        //Редактируем сообщение
        protected async void EditMessage(ChatId idChat, Int32 idMessage, string text) {
            await Task.Run(() => {
                try {
                    bot.EditMessageTextAsync(idChat
                       , idMessage
                       , text: text
                       , replyMarkup: keyboard
                       , disableWebPagePreview: true
                       , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html);
                } catch { }
            });
        }
        #endregion

    }
}
