﻿//Модуль действующего игрока. Содержит класс текущего игрока для учёта.
//

using System;
using Telegram.Bot.Types;

namespace TelegramGamesApp.Models {
    public class ActivePlayer :User{

        public int Score { get; set; }
        public int WeekScore { get; set; }
        public int MonthScore { get; set; }

        public int ChancesLeft { get; set; }
        public int WeekChances { get; set; }
        public int MonthChances { get; set; }

        public int LastMessageId { get; set; }
        public int LastChannelPostId { get; set; }
        public DateTime DateLastMessage { get; set; }


        //Создаём экземпляр действительного игрока
        public ActivePlayer(User user, int movesLeft) {
            Id = user.Id;
            FirstName = user.FirstName;
            LastName = user.LastName;
            ChancesLeft = movesLeft;
        }

    }
}