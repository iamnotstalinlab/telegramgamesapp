﻿// Модуль настроек скорой игры (Здесь запускаем быструю игрую и показываем её панель)
//

using Telegram.Bot.Types.ReplyMarkups;

namespace TelegramGamesApp.Models {
    public class QuickSettings :QuickGame {

        //Создаём экземпляр настроек скорой игры
        public QuickSettings(string botToken) :base(botToken) {}

        //Запускаем или завершаем скорую игру
        protected void LaunchOrFinishQuickGame() {
            if(IsActiveGame) {
               Finish(null);
            } else {
                Launch(true, null);
            }
        }

        //Показываем страницу скорой игры
        public void ShowQuickGamePage(long idUser) {

            string status = string.Empty;
            string buttonText = status;

            if(IsActiveGame) {

                status = "🏎  Запущена";
                buttonText = "Завершить сейчас";
            } else {

                status = "🏁 Завершена";
                buttonText = "Запустить сейчас";
            }

            text = "<i>Скорая игра</i>" +
                $"\nСостояние: <b>{status}</b>" +
                "\n\n<b>Пояснения:</b>" +
                "\n🎲  Режим выбирается случайно." +
                "\n🥇  Только 1 чемпион." +
                "\n⏰  Временем управляете сами." +
                "\n🍀  По 5 шансов у каждого игрока.";

            if(authors.ContainsKey(idQuickEditor)) {
                text += $"\n\n💡  <i>{authors[idQuickEditor].FirstName} {authors[idQuickEditor].LastName}" +
                    $" ID{authors[idQuickEditor].Id}</i>";
            }

            keyboard = new InlineKeyboardMarkup(new[]{
                new []{
                    InlineKeyboardButton.WithCallbackData(buttonText, "LaunchOrFinishQuickGameCallback")
                },

                new []{
                    InlineKeyboardButton.WithCallbackData("На главную", "ShowPlannedGamePageCallback")
                }});

            EditOrSendPage(idUser, text);
        }

        
    }
}
