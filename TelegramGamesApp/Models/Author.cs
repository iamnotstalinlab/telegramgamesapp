﻿//Модуль объекта автора инициативы.
//

using Telegram.Bot.Types;

namespace TelegramGamesApp.Models {
    public class Author {

        public long Id { get;}
        public string FirstName { get;}
        public string LastName { get; }

        public string SetCallback { get; set; }
        public int IdEditMessage { get; set; }

        //Создаём экземпляр автора
        public Author(User user) {
            Id = user.Id;
            FirstName = user.FirstName;
            LastName = user.LastName;
        }
    }
}
