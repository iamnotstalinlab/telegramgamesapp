﻿// Модуль планируемой игры, как надстройка над всеми остальными модулями: системы, настроек и скорой игры.
//

using System;
using System.Linq;
using System.Collections.Generic;
using TelegramGamesApp.Models;

using Telegram.Bot;
using Telegram.Bot.Types;
using System.Data.SQLite;

namespace TelegramGamesApp
{
    public class PlannedGame : QuickSettings
    {

        #region GLOBAL VARIABLES

        public DateTime StartTime { get; protected set; }
        public DateTime EndTime { get; protected set; }

        public string StartText { get; protected set; }
        public string EndText { get; protected set; }

        private DateTime gameWeek;
        private DateTime gameMonth;

        private Poll gamePoll;
        private int gamePollMessageId;
        public string gamePollId;

        public bool IsEnableGame;

        public PlannedGame(string botToken) : base(botToken){
            CreateTablePlannedGames(connection);
            OnQuickGameCreated += PlannedGame_OnQuickGameCreated;

            UpdateGameData();
        }


        public event EventHandler OnPlannedGameCreated;    //Событие когда создали плановую игру
        private void PlannedGame_OnQuickGameCreated(object sender, EventArgs e)
        {
            InitializeAndSavePlannedGameTable(idCurrentUser);
            UpdateGameData();
            OnPlannedGameCreated?.Invoke(this, new EventArgs());
        }

        #endregion

        #region UPDATE SECTION

        //Обновляем игровые данные
        public void UpdateGameData()
        {
            LoadPlannedGameData();
            SetTimer(StartTime, true);
        }
        #endregion

        #region ACTIVE SECTION


        //Запускаем или останавливаем планируемую игру, если экстренный случай
        bool tEnable;
        protected void LaunchOrFinishQuickGameThrowPlannedGame(long idEditor)
        {

            IdEditor = idEditor;
            if (IsEnableGame)
            {
                tEnable = true;
                DisableGame(false);
            }

            if (!IsActiveGame)
            {
                Launch(true, null);
            }
            else
            {
                Finish(null);

                if (tEnable)
                {
                    EnableGame(true);
                    tEnable = false;
                }
            }
        }


        //События при включении и отключении
        public event EventHandler OnEnableGame;
        public event EventHandler OnDisableGame;

        //Включаем планируемую игру
        public void EnableGame(bool isEventInvoke)
        {
            IsEnableGame = true;
            SetTimer(StartTime, true);

            if (isEventInvoke)
            { //Если касается
                OnEnableGame?.Invoke(this, new EventArgs());
            }
        }

        //Отключаем планируемую игру
        public void DisableGame(bool isEventInvoke)
        {
            IsEnableGame = false;

            if (timer != null)
            {
                timer.Stop();
                timer = null;
            }

            if (isEventInvoke)
            {
                OnDisableGame?.Invoke(this, new EventArgs());
            }

        }

        //Устанавливаем счётчик до конкретного времени
        double interval;
        System.Timers.Timer timer;

        private void SetTimer(DateTime dateTime, bool isStart)
        {
            if (IsEnableGame)
            {
                if (timer == null)
                {
                    timer = new System.Timers.Timer();
                    timer.Elapsed += Timer_Elapsed;
                }

                //Вычисляю интервал до следующего вызова по сигналу, если меньше чем сегодня, то переношу сигнал на завтра
                if (dateTime < DateTime.Now)
                {

                    while (dateTime < DateTime.Now){
                        dateTime = dateTime.AddDays(1);
                    }
                    if (IdEditor != 0 && isStart){
                        StartTime = dateTime;
                        SaveStartTime();
                    }
                }
                interval = (dateTime - DateTime.Now).TotalMilliseconds;
                timer.Interval = interval;
                timer.Start();
            }
        }

        //Запускаем и останавливаем планируемую игру
        string regularText;
        DateTime testTime;
        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (IsEnableGame)
            {
                if (!IsActiveGame)
                {

                    SetTimer(EndTime, false);
                    StopGamePoll();

                    regularText = $"Плановая игра в {Mode}." +
                        $"\n<i>(Чтобы играть, пришлите смайлик {Mode} в комментарии к этой записи)</i>" +
                        $"\n\n<i>{StartText}</i>\n\n" +
                        "<b>Пояснения:</b>" +
                        $"\n🕰  Игра с {StartTime.ToString("HH:mm")} до {EndTime.ToString("HH:mm")}" +
                        $"\n🥇  {MaxWinners} чемпион(а/ов)." +
                        $"\n🍀  По {StartChances} шанс(а/ов) у каждого игрока.";

                    Launch(false, regularText);
                }
                else
                {

                    SetTimer(StartTime, true);

                    regularText = "";
                    regularText += $"<i>{EndText}</i>";
                    Finish(regularText);

                    SendGamePoll();
                    ResetStatistics();
                }
            }
            else
            {
                if (timer != null)
                {
                    timer.Stop();
                    timer = null;
                }
            }
        }

        #endregion

        #region STATISTIC SECTION

        //Сбрасываем статистику
        private void ResetStatistics()
        {

            DateTime now = DateTime.Now;
            if (((now.Day - gameWeek.Day) > 6)
                || now.DayOfWeek == DayOfWeek.Sunday)
            {
                SendResultOfPeriod(gameWeek, now, "WeekScore");
                ResetActivePlayersResultsForPeriod(new List<string> { "WeekScore", "WeekChances" });

                ResetStartGamePeriod("GameWeek", now);
                UpdateGameData();
            }

            if (now.AddDays(1).Month > gameMonth.Month)
            {
                SendResultOfPeriod(gameMonth, now, "MonthScore");
                ResetActivePlayersResultsForPeriod(new List<string> { "MonthScore", "MonthChances" });

                ResetStartGamePeriod("GameMonth", now);
                UpdateGameData();
            }
        }

        //Отправляем в канал результаты за указнный временной период
        private void SendResultOfPeriod(DateTime firstDate, DateTime secondDate, string scoreName)
        {

            periodResults = GetLeaderboardForPeriod(scoreName);
            if (periodResults != null && periodResults.Count > 0)
            {


                string text = $"Лучшие игроки";
                switch (scoreName)
                {
                    case "WeekScore":
                        text += "<b> недели</b>:";
                        break;
                    case "MonthScore":
                        text += "<b> месяца</b>:";
                        break;
                }

                for (int i = 0; i < periodResults.Count() && i < 5; i++)
                {
                    text += $"\n\n{i + 1}) " +
                        $"<a href=\"{GetLinkFromMessageId(GameChatId, periodResults[i].LastMessageId)}" +
                        $"?thread={ChatTitleMessageId} \">" +
                        $"{periodResults[i].FirstName} {periodResults[i].LastName}. </a>[ID {periodResults[i].Id}]";


                    switch (scoreName)
                    {
                        case "WeekScore":
                            text += $"\n<b>{periodResults[i].WeekScore}</b> балл(а/ов)" +
                                $" за <i>{periodResults[i].WeekChances}</i> шанс(а/ов)";
                            break;
                        case "MonthScore":
                            text += $"\n<b>{periodResults[i].MonthScore}</b> балл(а/ов)" +
                                $" за <i>{periodResults[i].MonthChances}</i> шанс(а/ов)";
                            break;
                    }

                }
                if (periodResults.Count > 5)
                {
                    text += "\n...";
                }

                text += $"\n\n<i>(Результаты за период с {firstDate.ToString("dd/MM/yyyy")} " +
                    $"по {secondDate.ToString("dd/MM/yyyy")})</i>";

                SendChannelMessage(text, null, true, true);
            }
        }

        //Получаем игровую статистику за определённый временной период
        List<ActivePlayer> periodResults;
        public List<ActivePlayer> GetLeaderboardForPeriod(string scoreName)
        {
            periodResults = LoadActivePlayersResults(scoreName, StartChances);

            if (periodResults != null)
            {

                //Сначала сортируем по дате отправленного сообщения. (Чем раньше тем лучше)
                periodResults = periodResults.OrderBy(o => o.DateLastMessage).ToList();

                switch (scoreName)
                {

                    case "WeekScore":

                        //Теперь сортируем по сделанным шансам. (Чем больше шансов выполнили, тем лучше)
                        periodResults = periodResults.OrderByDescending(o => o.WeekChances).ToList();

                        //И тут сортируем по баллам (Чем больше баллов, тем лучше)
                        periodResults = periodResults.OrderByDescending(o => o.WeekScore).ToList();
                        break;

                    case "MonthScore":

                        periodResults = periodResults.OrderByDescending(o => o.MonthChances).ToList();
                        periodResults = periodResults.OrderByDescending(o => o.MonthScore).ToList();
                        break;
                }
            }
            return periodResults;
        }

        #endregion

        #region POLL SECTION

        //Отправляем действующий опрос на желаемый режим планируемой игры
        private void SendGamePoll()
        {

            Message pollMessage = bot.SendPollAsync(
                chatId: GameChannelId,
                question: "Планируем ближайшую игру:"
                , options: new[]{
                    "🎯 Дартс",
                    "🎲 Кубик",
                    "🏀 Мяч",
                    "🎳 Боулинг",
                    "🎰 Игровой аппарат"},
                isAnonymous: true).Result;

            gamePoll = pollMessage.Poll;
            gamePollId = gamePoll.Id;
            gamePollMessageId = pollMessage.MessageId;
            SaveGamePollMessageId(gamePollMessageId, gamePollId);
        }

        //Останавливаем опрос вручную и записываем его для учёта результатов
        public void ManualStopGamePoll(Poll poll)
        {
            if (poll.Id == gamePollId)
            {
                gamePoll = poll;
            }
        }

        //Останавливаем опрос автоматически и учитываем его результаты
        private void StopGamePoll()
        {
            if (gamePollMessageId != 0)
            {

                if (gamePoll != null)
                {
                    if (!gamePoll.IsClosed)
                    {
                        gamePoll = bot.StopPollAsync(GameChannelId, gamePollMessageId).Result;
                    }
                }
                else
                {
                    //Вообще бред какой-то тут: если опрос есть, то останавливаем, если его нет, то тоже останавливаемЫ
                    try {
                        gamePoll = bot.StopPollAsync(GameChannelId, gamePollMessageId).Result;
                    } catch { }
                }
                SetModeFromGamePollResult();
            }
        }

        //Устанавливаем игровой режим на основе результата опроса
        private void SetModeFromGamePollResult()
        {
            if (gamePoll != null)
            {

                int maxVoteIndex = 0;
                List<PollOption> options = gamePoll.Options.ToList();

                for (int i = 0; i < options.Count(); ++i)
                {
                    if (options[i].VoterCount > options[maxVoteIndex].VoterCount)
                    {
                        maxVoteIndex = i;
                    }
                    else
                    {

                        //При одинаковом количестве голосов выбираем случайный из них
                        if (options[i].VoterCount == options[maxVoteIndex].VoterCount)
                        {
                            if (new Random().Next(0, 2) == 1)
                            {
                                maxVoteIndex = i;
                            }
                        }
                    }
                }

                switch (maxVoteIndex)
                {
                    case 0:
                        Mode = "🎯";
                        break;
                    case 1:
                        Mode = "🎲";
                        break;
                    case 2:
                        Mode = "🏀";
                        break;
                    case 3:
                        Mode = "🎳";
                        break;
                    case 4:
                        Mode = "🎰";
                        break;
                }

                gamePollMessageId = 0;
                gamePollId = "NULL";
                gamePoll = null;
                SaveGamePollMessageId(gamePollMessageId, gamePollId);
            }
        }

        #endregion


        #region DATA BASE

        //Создаём таблицу действующих игр
        private void CreateTablePlannedGames(SQLiteConnection cnct)
        {

            lock (dbLocker)
            {
                OpenDatabase();
                string sCommand = @"CREATE TABLE IF NOT EXISTS PlannedGames (
	                    Id	INTEGER NOT NULL UNIQUE,
                        IdPlannedEditor INTEGER NOT NULL,
	                    StartTime	TEXT NOT NULL DEFAULT '08:35',
	                    EndTime	TEXT NOT NULL DEFAULT '20:35',
	                    StartChances	INTEGER NOT NULL DEFAULT 5,
	                    MaxWinners	INTEGER NOT NULL DEFAULT 1,
	                    StartText	TEXT DEFAULT 'Это текст в объявлении игры.',
	                    EndText	TEXT DEFAULT 'Это текст в титрах игры.',
                        GameWeek    TEXT,
                        GameMonth   TEXT,
                        GamePollMessageId INTEGER,
                        GamePollId TEXT,
                        PRIMARY KEY(Id AUTOINCREMENT));";

                SQLiteCommand command = new SQLiteCommand();

                command.Connection = cnct;
                command.CommandText = sCommand;
                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }


        //Создаём таблицу для планируемой игры
        private void InitializeAndSavePlannedGameTable(long idPlannedEditor)
        {

            lock (dbLocker)
            {
                OpenDatabase();
                string sCommand = "INSERT INTO PlannedGames" +
                    "(IdPlannedEditor, StartTime, EndTime, StartChances, MaxWinners, StartText, EndText, GameWeek, GameMonth" +
                    ", GamePollMessageId, GamePollId) VALUES (@idPlannedEditor, @startTime, @endTime, @startChances, @maxWinners" +
                    ", @startText, @endText, @gameWeek, @gameMonth, @gamePollMessageId, @gamePollId)";

                SQLiteCommand command = new SQLiteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;

                command.Parameters.AddWithValue("@idPlannedEditor", idPlannedEditor);
                command.Parameters.AddWithValue("@startTime", "08:35");
                command.Parameters.AddWithValue("@endTime", "20:35");
                command.Parameters.AddWithValue("@startChances", 5);
                command.Parameters.AddWithValue("@maxWinners", 1);
                command.Parameters.AddWithValue("@startText", "Это текст в объявлении турнира");
                command.Parameters.AddWithValue("@endText", "Это текст в титрах турнира");
                command.Parameters.AddWithValue("@gameWeek", DateTime.Now.ToString());
                command.Parameters.AddWithValue("@gameMonth", DateTime.Now.ToString());
                command.Parameters.AddWithValue("@gamePollMessageId", 0);
                command.Parameters.AddWithValue("@gamePollId", "NULL");
                command.ExecuteNonQuery();

                CloseDatabase();
            }
        }

        //Получаем данные для проведения обычной игры
        private void LoadPlannedGameData()
        {

            lock (dbLocker)
            {
                OpenDatabase();
                string sCommand = "SELECT * FROM PlannedGames";

                SQLiteCommand command = new SQLiteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;

                dataReader = command.ExecuteReader();
                if (dataReader.HasRows)
                {

                    while (dataReader.Read())
                    {
                        int idPlannedEditor = int.Parse(dataReader["IdPlannedEditor"].ToString());

                        StartTime = DateTime.Parse(dataReader["StartTime"].ToString());
                        EndTime = DateTime.Parse(dataReader["EndTime"].ToString());
                        StartChances = int.Parse(dataReader["StartChances"].ToString());
                        MaxWinners = int.Parse(dataReader["MaxWinners"].ToString());
                        StartText = dataReader["StartText"].ToString();
                        EndText = dataReader["EndText"].ToString();

                        gameWeek = DateTime.Parse(dataReader["GameWeek"].ToString());
                        gameMonth = DateTime.Parse(dataReader["GameMonth"].ToString());

                        gamePollMessageId = int.Parse(dataReader["GamePollMessageId"].ToString());
                        gamePollId = dataReader["GamePollId"].ToString();
                    }
                }

                dataReader.Close();
                CloseDatabase();
            }
        }

        //Устанавливаем данные голосования планируемой игры
        private void SaveGamePollMessageId(int pollMessageId, string pollId)
        {

            lock (dbLocker)
            {
                OpenDatabase();
                string sCommand = "UPDATE PlannedGames SET GamePollMessageId =@gamePollMessageId, GamePollId =@pollId";

                SQLiteCommand command = new SQLiteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@gamePollMessageId", pollMessageId.ToString());
                command.Parameters.AddWithValue("@pollId", pollId);

                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }

        //Устанавливаем время начала планируемой игры
        protected void SaveStartTime()
        {

            lock (dbLocker)
            {
                OpenDatabase();

                string sCommand = "UPDATE PlannedGames SET StartTime =@startTime, IdPlannedEditor =@idPlannedEditor WHERE Id =1";

                SQLiteCommand command = new SQLiteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@startTime", StartTime.ToString("HH:mm"));
                command.Parameters.AddWithValue("@idPlannedEditor", IdEditor);

                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }

        //Устанавливаем время завершения планируемой игры
        protected void SaveEndTime()
        {

            lock (dbLocker)
            {
                OpenDatabase();

                string sCommand = "UPDATE PlannedGames SET EndTime =@endTime, IdPlannedEditor =@idPlannedEditor WHERE Id =1";

                SQLiteCommand command = new SQLiteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@endTime", EndTime.ToString("HH:mm"));
                command.Parameters.AddWithValue("@idPlannedEditor", IdEditor);

                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }



        //Сбрасываем время отсчёта для игрового периода
        private void ResetStartGamePeriod(string perodName, DateTime startPeriod)
        {

            lock (dbLocker)
            {
                OpenDatabase();
                string sCommand = $"UPDATE PlannedGames SET {perodName} =@startPeriod WHERE Id =1";

                SQLiteCommand command = new SQLiteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;

                command.Parameters.AddWithValue("@startPeriod", startPeriod.ToString());
                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }

        //Сбрасываем результаты действующих игроков за определённый период
        private void ResetActivePlayersResultsForPeriod(List<string> namesResults)
        {

            lock (dbLocker){
                if (namesResults != null && namesResults.Count > 0){

                    OpenDatabase();
                    string sCommand = $"UPDATE ActivePlayers SET {namesResults[0]} =0";
                    for (int i = 1; i < namesResults.Count; ++i)
                    {
                        sCommand += $", {namesResults[i]} =0";
                    }

                    SQLiteCommand command = new SQLiteCommand();
                    command.CommandText = sCommand;
                    command.Connection = connection;

                    command.ExecuteNonQuery();

                    command.CommandText = sCommand;
                    command.ExecuteNonQuery();
                    CloseDatabase();
                }
            }
        }


        #endregion

    }
}
