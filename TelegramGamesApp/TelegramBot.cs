﻿//Модуль который создаёт экземпляр игры и принимает все изменения со стороны пользователей Телеграм
//

using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using TelegramGamesApp.Models;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Extensions.Polling;
using System.Threading;
using System;

namespace TelegramGamesApp{
    public class TelegramBot{

        #region CREATE BLOCK
        PlannedSettings game;

        public TelegramBot(string botToken){
            game = new PlannedSettings(botToken);
            game.bot.StartReceiving(new DefaultUpdateHandler(HandleUpdateAsync, HandleErrorAsync));
        }



        private async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
        {
            if (update.Type == UpdateType.Message){
                AcceptAnyMessage(update.Message);
            }else{
                if (update.Type == UpdateType.CallbackQuery){
                    AcceptAnyCallbackQuery(update.CallbackQuery);
                }else{
                    AcceptAnyUpdate(update);
                }
            }
        }

        #endregion

        #region TELEGRAM EVENTS




        //Принимаем любые сообщения
        private async void AcceptAnyMessage(Message message){
            await Task.Run(() => {
                if (message != null){
                    if (!game.IsSystemEmpty
                            && message.Chat.Id == game.GameChatId){
                        game.AcceptChatMessage(message);
                    }else{
                        game.AcceptMessage(message);
                    }
                }
            });
        }

        //Принимаем любые обратные вызовы
        private async void AcceptAnyCallbackQuery(CallbackQuery callbackQuery){
            await Task.Run(() => {
                if (callbackQuery != null){
                    if (game != null){
                        game.AcceptCallback(callbackQuery);
                    }
                }
            });
        }

        //Принимаем любое обновление
        private async void AcceptAnyUpdate(Update update){
            await Task.Run(() => {
                if (update.Type == UpdateType.Poll
                    && update.Poll.Id == game.gamePollId
                    && update.Poll.IsClosed){

                    game.ManualStopGamePoll(update.Poll);
                }
            });
        }

        async Task HandleErrorAsync(ITelegramBotClient arg1, Exception exception, CancellationToken cancellationToken){
            try{
                game.WriteLog("\n");
                game.WriteLog("Дата и время ошибки: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm"));
                game.WriteLog(exception.Message);
                game.WriteLog(exception.Data.ToString());
                game.WriteLog(exception.HResult.ToString());
                game.WriteLog(exception.StackTrace);
                game.WriteLog(exception.Source);
                game.WriteLog("\n");
            }catch { }
        }

        #endregion

    }
}
