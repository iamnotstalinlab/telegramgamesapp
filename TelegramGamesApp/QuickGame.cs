﻿// Модуль скорой игры. Шаблон и основа для планируемых игр.
//

using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.ReplyMarkups;
using TelegramGamesApp.Models;

namespace TelegramGamesApp
{
    public class QuickGame : Models.System
    {

        #region GLOBAL VARIABLES

        public int StartChances { get; protected set; }    //Максимальное число бросков
        public int MaxWinners { get; protected set; }   //Максимальное число победителей

        public String Mode { get; protected set; }      //Режим игры

        public int ChatTitleMessageId { get; set; }     //Номер сообщения с объявлением игры
        public int ChannelTitleMessageId { get; set; }  //Номер комментария в котором запущена игра

        public bool IsActiveGame;                           //Флаг проверки действует ли сейчас игра

        System.Timers.Timer statisticsTimer = new System.Timers.Timer();            
        const int statisticsInterval = 15 * 60 * 1000;  //Таймер статистики раз в 15 мин. обновление

        private int sChances; //Начальное количество шансов в текущей игре (используем только в этом модуле)
        private int mWinners; //Максимальное число победителей в текущей игре (используем только в этом модуле)
        #endregion

        #region CREATE GAME

        //Создаём экземпляр быстрой игры
        public QuickGame(string botToken) : base(botToken){

            mWinners = 1;
            sChances = 5;

            Mode = CreateRandomMode();
            statisticsTimer.Interval = statisticsInterval;
            statisticsTimer.Elapsed += BreakTimer_Elapsed;

            OnSystemSettingsCreated += QuickGameOnSystemSettingsCreated;
        }


        public event EventHandler OnQuickGameCreated;    //Событие при изменении системных настроек
        private void QuickGameOnSystemSettingsCreated(object sender, EventArgs e)
        {
            CreateTableActivePlayers(connection);
            OnQuickGameCreated?.Invoke(this, new EventArgs());
        }



        #endregion

        #region LAUNCH GAME

        //Запускаем игру
        string titleText = string.Empty;
        public event EventHandler OnActivateGame;   //Событие при запуске

        public void Launch(bool isQuick, string regularText){

            if (isQuick)
            {
                mWinners = 1;
                sChances = 5;

                Mode = CreateRandomMode();
                titleText = $"<i>Скорая игра в</i> {Mode}" +
                    $"\n<i>(Чтобы играть, пришлите смайлик {Mode} в комментарии к этой записи)</i>" +
                    "\n\n<b>Пояснения:</b>" +
                    "\n🎲  Режим выбран случайно." +
                    "\n🥇  Только 1 чемпион." +
                    "\n🍀  По 5 шансов у каждого игрока.";
            }
            else
            {
                mWinners = MaxWinners;
                sChances = StartChances;
                titleText = regularText;
            }

            IsActiveGame = true;
            OpenOrCloseChat(true);
            ResetResults(sChances);

            finishedPlayers = 0;
            periodResults = null;
            statisticsTimer.Start();

            ReplyKeyboardMarkup replyKeyboard = new ReplyKeyboardMarkup(new KeyboardButton(Mode));
            replyKeyboard.ResizeKeyboard = true;

            //Присылаю сообщение открывающее турнир
            Message msg = SendChannelMessage(titleText, null, false, true);
            OnActivateGame?.Invoke(this, new EventArgs());
        }

        //Сбрасываем прежние результаты
        private void ResetResults(int chances)
        {

            //Открываем доступ пользователям завершившим прежнюю игру
            List<ActivePlayer> oldPlayers = LoadActivePlayersResults("Score", sChances);

            if (oldPlayers != null)
            {
                for (int i = 0; i < oldPlayers.Count; ++i)
                {
                    OpenUser(oldPlayers[i].Id);
                }
            }

            //Сбрасываем результаты прежней игры
            ResetActivePlayersResults(chances);
        }

        //Создаём случайный режим игры
        private string CreateRandomMode()
        {

            Random random = new Random();
            int rnd = random.Next(0, 5);
            switch (rnd)
            {
                case 0:
                    return "🎲";
                case 1:
                    return "🎯";
                case 2:
                    return "🏀";
                case 3:
                    return "🎳";
                case 4:
                    return "🎰";
                default:
                    return "🎲";
            }
        }

        //Принимаем сообщение в чате
        public async void AcceptChatMessage(Message message){
            GameChat = message.Chat;

            if (IsActiveGame){
                if (message.ForwardFromChat != null){
                    AcceptChannelMessage(message);
                }else{
                    if (message.Type == MessageType.Dice
                        && message.ReplyToMessage != null){
                        ExecuteAction(message);
                    }else{

                        //Проверяем кто прислал сообщение. Если это не администратор, то удаляем
                        await Task.Run(() => {
                            List<ChatMember> admins = bot.GetChatAdministratorsAsync(GameChannelId).Result.ToList();
                            ChatMember tMember = admins.Find(item => item.User.Id == message.From.Id);
                            if (tMember == null
                                && message.From.Username != "GroupAnonymousBot"){
                                DeleteMessage(message.MessageId);
                            }
                        });
                    }
                }
            }
        }



        //Принимаем сообщение из чата как главное объявление на которое будем ссылаться
        protected void AcceptChannelMessage(Message message)
        {
            if (ChannelTitleMessageId == 0 && ChatTitleMessageId == 0){

                if (((message.ForwardFromChat.Id) == GameChatId)
                    || ((message.ForwardFromChat.Id) == GameChannelId)){

                    ChatTitleMessageId = message.MessageId;
                    if (message.ForwardFromMessageId != null){
                        ChannelTitleMessageId = (int)message.ForwardFromMessageId;
                    }

                    ReplyKeyboardMarkup replyKeyboard = new ReplyKeyboardMarkup(new KeyboardButton(Mode));
                    replyKeyboard.ResizeKeyboard = true;
                    string msgText = $"Как играть? Очень легко. " +
                        $"\nПросто пришлите смайлик {Mode} для участия." +
                        $"\n\n<i>Или отправьте касанием стикера в чате</i>" +
                        $"\nПри трудностях - ждём вас в общем чате.";

                    SendChatMessage(msgText, replyKeyboard, false, ChatTitleMessageId, true);
                    bot.PinChatMessageAsync(GameChannelId, (int)ChannelTitleMessageId);
                    bot.SendDiceAsync(GameChatId, GetModeDice(), true, null, ChatTitleMessageId);
                }
            }
        }

        protected Emoji GetModeDice()
        {
            switch (Mode)
            {
                case "🎯":
                    return Emoji.Darts;
                case "🏀":
                    return Emoji.Basketball;
                case "🎲":
                    return Emoji.Dice;
                case "🎳":
                    return Emoji.Bowling;
                case "🎰":
                    return Emoji.SlotMachine;
            }
            return Emoji.Dice;
        }

        #endregion

        #region ACTIVE SECTION

        //Выполняем игровое действие
        public async void ExecuteAction(Message message)
        {

            await Task.Run(() => {
                if ((message.Dice.Emoji == Mode)
                && message.ReplyToMessage.MessageId == ChatTitleMessageId
                && message.From.Username != "Channel_Bot" && message.From.Username != "GroupAnonymousBot"){

                    //Сделал данный блок, чтобы потоки не мешали друг другу
                    ActivePlayer current;
                    current = LoadActivePlayer(message.From.Id);

                    if (current == null){
                        current = new ActivePlayer(message.From, sChances);
                    }

                    if (current.ChancesLeft > 0){

                        //Переписываю имя и фамилию, если изменились, чтобы не было недопонимания
                        if (current.FirstName != message.From.FirstName){
                            current.FirstName = message.From.FirstName;
                        }

                        if (current.LastName != message.From.LastName){
                            current.LastName = message.From.LastName;
                        }

                        int score = CalculateScore(message.Dice.Value);
                        int extraChances = CalculateExtraChances(message.Dice.Value);

                        current.Score += score;
                        current.WeekScore += score;
                        current.MonthScore += score;
                        current.ChancesLeft += extraChances;

                        --current.ChancesLeft;
                        ++current.WeekChances;
                        ++current.MonthChances;

                        string text = BuildActionText(current, score, extraChances, message.MessageId);

                        current.DateLastMessage = message.Date.ToLocalTime();
                        current.LastMessageId = message.MessageId;

                        if (current.ChancesLeft == 0){
                            text += " <b>выполнил</b>(а) свою <b>игру</b>.";
                            CloseUser(current.Id);
                        }

                        SaveActivePlayerResults(current);
                        SendChatMessage(text, null, true, ChatTitleMessageId, true);
                    }
                }else{

                    if (message.Text == Mode){
                        string text = $"⚠️ Я не умею считать такие броски." +
                            $"\nЛучше всего - бросать касаясь стикеров в чате." +
                            "\n\n<i>При трудностях - ждём вас в общем чате.</i>";
                        SendChatMessage(text, null, false, message.MessageId, true);
                    }else{
                        DeleteMessage(message.MessageId);
                    }
                }
            });
        }

        //Собираю текст текст основного действия
        private string BuildActionText(ActivePlayer player, int score, int extraChances, int messageId)
        {

            string text = string.Empty;
            text += $"<a href=\"{GetLinkFromMessageId(GameChatId, messageId)}" +
                    $"?thread={ChatTitleMessageId}\">" +
                    $"⭐️  <b>+{score} балл</b>(а/ов)</a>";

            if (extraChances > 0){
                text +=$" \n🍀  <b>+{extraChances}</b> шанс(а/ов)";
            }

            text += $"\n\n💼  Уже <b>{player.Score} балл</b>(а/ов)" +
                $"\n🎄  Есть ещё <b>{player.ChancesLeft} шанс</b>(а/ов)";

            //Убираю знак '@'в начале адреса чата
            text += $"\n💡  <i><a href=\"{GetLinkFromMessageId(GameChatId, player.LastMessageId)}" +
                $"?thread={ChatTitleMessageId}\">Предыдущий шанс</a></i>";


            text += $"\n\n🧸  Для {player.FirstName} [ID {player.Id}]";
            return text;
        }


        //Обновляем таблицу рекордов в записи за определённое время
        private void BreakTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (IsActiveGame){
                UpdateLeaderboard();
            } else {
                if (statisticsTimer != null){
                    statisticsTimer.Stop();
                }
            }
        }

        //Обновляем запись канала с списком лидеров
        List<ActivePlayer> periodResults;

        private String GetLeaderboardText(Int32 leadersCount){

            string result = null;
            periodResults = GetLeaderboard();

            if (periodResults != null && periodResults.Count > 0) {

                result = $"\n\n👑 <b>Баллы набрали:</b>\n\n";
                for (int i = 0; i < periodResults.Count() && i < leadersCount; i++) {
                    result += $"<b>{i + 1})</b> " +

                    $"<a href=\"{GetLinkFromMessageId(GameChatId, periodResults[i].LastMessageId)}" +
                    $"?thread={ChatTitleMessageId}\">" +
                    $"{periodResults[i].FirstName} {periodResults[i].LastName}</a>";

                    if (periodResults[i].ChancesLeft != 0) {
                        result += $"\nСчёт: <b>{periodResults[i].Score}</b>\n\n";
                    } else {

                        //Зачёркиваем фразу "счёт", для игроков, которые выполнили все броски
                        result += $"\n<s>Счёт:</s> <b>{periodResults[i].Score}</b>\n\n";
                    }
                }

                if (periodResults.Count > leadersCount) {
                    result += $"и ещё {periodResults.Count -leadersCount} участник(а/ов) ...\n\n";
                }
            }
            return result;
        }


        //Обновляем таблицу рекордов в игровой записи
        //Если победителей меньше трёх то присылаем статистику по тройке лидеров.
        //Если победителей планируется больше, то присылаем по их количеству

        private void UpdateLeaderboard() {

            Int32 sCount = 3;
            if (mWinners > 3 && mWinners < 10) {
                sCount = mWinners;
            } else {
                if (mWinners > 9) {
                    sCount = 9;
                }
            }

            string leaderboardText = GetLeaderboardText(sCount);
            if (!string.IsNullOrEmpty(leaderboardText)) {

                leaderboardText += $"<i>*Список на {DateTime.Now.ToString("HH:mm")}" +
                    $" (Обновляем раз в {(statisticsInterval / 60000).ToString()} мин.)</i>";

                leaderboardText = titleText.Insert(titleText.IndexOf("\n\n<b>Пояснения:</b>"), leaderboardText);
                EditChannelMessage(ChannelTitleMessageId, $"{leaderboardText}", true);
            }
        }

        //Завершаем текущую игру
        public event EventHandler OnFinishGame; //События при запуске игры и окончании

        public void Finish(string regularEndText)
        {

            statisticsTimer.Stop();     //Останавливаем посылки статистики. Заключительную таблицу пришлём в этом методе ниже.
            if (IsActiveGame){

                string finishText = "\n\n🏆  <b>Поздравляем с победой</b>:";
                string statisticText = string.Empty;

                periodResults = GetLeaderboard();
                if (periodResults != null){

                    //Вычисляем кколичество победителей, к поздравлению
                    int count = mWinners;
                    if (periodResults.Count() < mWinners){
                        count = periodResults.Count();
                    }

                    for (int i = 0; i < count; ++i){

                        finishText += $"\n\n{periodResults[i].FirstName} {periodResults[i].LastName} <a href=\"{GetLinkFromMessageId(GameChatId, periodResults[i].LastMessageId)}" +
                            $"?thread={ChatTitleMessageId}\">[ID {periodResults[i].Id}]</a>" +
                            $"\nРезультат: <b>{periodResults[i].Score}</b>";
                    }
                    statisticText = $"Шансы выполнили: <b>{finishedPlayers}</b>/{periodResults.Count}";
                }

                if (!string.IsNullOrEmpty(regularEndText)){
                    finishText += "\n\n";
                    finishText += regularEndText;
                }

                finishText += $"\n\n<i>Данная игра закончилась.</i>\n{statisticText}";



                if (mWinners < 3) {     //Присылаем заключительную таблицу игры если победителей больше трёх
                                        //Если победителей 3 и больше, тополучается дублирование

                    string leaderboardText =GetLeaderboardText(3);      
                    if (!string.IsNullOrEmpty(leaderboardText)) {

                        Int32 sIndex = titleText.IndexOf("\n\n<b>Пояснения:</b>");

                        titleText = titleText.Remove(sIndex, 2);                   
                        leaderboardText = titleText.Insert(sIndex, leaderboardText);

                        EditChannelMessage(ChannelTitleMessageId, $"{leaderboardText}{finishText}", true);
                    } 
                } else {
                    EditChannelMessage(ChannelTitleMessageId, $"{titleText}{finishText}", true);
                }

                SendChatMessage(finishText, null, false, ChatTitleMessageId, true);
                bot.UnpinChatMessageAsync(GameChannelId);
                OpenOrCloseChat(false);

                IsActiveGame = false;
                ChatTitleMessageId = 0;
                ChannelTitleMessageId = 0;

                OnFinishGame?.Invoke(this, new EventArgs());
            }
        }

        #endregion

        #region GAME CALCULATION

        //Считаем результат броска соответственно игровому режиму
        private int CalculateScore(int score)
        {

            switch (Mode)
            {
                case "🎯":
                    CalculateDartsScore(ref score);
                    break;
                case "🏀":
                    CalculateBasketballScore(ref score);
                    break;
                case "🎲":
                    CalculateDiceScore(ref score);
                    break;
                case "🎳":
                    CalculateBowlingScore(ref score);
                    break;
            }
            return score;
        }

        //Считаем результат в режиме "Дартс"
        private int CalculateDartsScore(ref int score)
        {
            switch (score)
            {
                case 6:
                    score = 10;
                    break;
                case 5:
                    score = 9;
                    break;
                case 4:
                    score = 8;
                    break;
                case 3:
                    score = 7;
                    break;
                case 2:
                    score = 6;
                    break;
                case 1:
                    score = 0;
                    break;
            }

            return score;
        }

        //Считаем результат в режиме баскетбол
        private int CalculateBasketballScore(ref int score)
        {
            if (score > 3)
            {
                score = 1;
            }
            else
            {
                score = 0;
            }
            return score;
        }

        //Считаем результат в режиме "Кости"
        private int CalculateDiceScore(ref int score)
        {
            return score;
        }

        //Считаем результат в режиме "Боулинг"
        private int CalculateBowlingScore(ref int score)
        {
            if (score < 3)
            {
                score--;
            }
            return score;
        }

        //Считаем дополнительные шансы
        private int CalculateExtraChances(int score)
        {

            switch (Mode)
            {
                case "🎯":
                    return CalculateDartsExtraChances(score);
                case "🏀":
                    return CalculateBasketballExtraChances(score);
                case "🎲":
                    return CalculateDiceExtraChances(score);
                case "🎳":
                    return CalculateBowlingExtraChances(score);
                case "🎰":
                    return CalculateSlotmachineExtraChances(score);
            }
            return 0;
        }

        //Считаем дополнительные шансы для "Дартс"
        private int CalculateDartsExtraChances(int score)
        {

            if (score == 6)
            {
                return 1;
            }
            return 0;
        }

        //Считаем дополнительные шансы для режима "Мяч"
        private int CalculateBasketballExtraChances(int score)
        {

            if (score > 3)
            {
                return 1;
            }
            return 0;
        }

        //Считаем дополнительные шансы для режима "Кости"
        private int CalculateDiceExtraChances(int score)
        {
            if (score == 6)
            {
                return 1;
            }
            return 0;
        }

        //Считаем дополнительные шансы для режима "Боулинг"
        private int CalculateBowlingExtraChances(int score)
        {
            if (score == 6)
            {
                return 1;
            }
            return 0;
        }

        //Считаем дополнительные шансы для режима "Игровой автомат"
        private int CalculateSlotmachineExtraChances(int score)
        {
            if (score == 1
                || score == 22
                || score == 43
                || score == 64)
            {

                return 1;
            }
            return 0;
        }

        //Получаем игровые результаты в таблице-рекордов
        int finishedPlayers;
        public List<ActivePlayer> GetLeaderboard()
        {

            finishedPlayers = 0;
            periodResults = LoadActivePlayersResults("Score", sChances);

            if (periodResults != null)
            {

                //Сначала сортируем по дате отправленного сообщения. (Чем раньше тем лучше)
                periodResults = periodResults.OrderBy(o => o.DateLastMessage).ToList();

                //Теперь сортируем по сделанным шансам. (Чем меньше шансов осталось, тем лучше)
                periodResults = periodResults.OrderBy(o => o.ChancesLeft).ToList();

                //И наконец сортируем по баллам. (Чем больше тем лучше) День/Неделя/Месяц - по аналогии
                periodResults = periodResults.OrderByDescending(o => o.Score).ToList();

                //Считаем сколько игроков завершили свои броски
                foreach (ActivePlayer ap in periodResults)
                {
                    if (ap.ChancesLeft == 0)
                    {
                        ++finishedPlayers;
                    }
                }
            }
            return periodResults;
        }
        #endregion

        #region GAME PERMISSION

        //Открываем или закрываем чат
        ChatPermissions permissions;
        private async void OpenOrCloseChat(bool isOpen)
        {

            if (isOpen)
            {
                permissions = GetOpenPermissions();
            }
            else
            {
                permissions = GetClosePermission();
            }

            // Функция SetChatPermissionAsync не всегда выполняется. Поэтому я проверяю её
            //и выполняю до тех пор, пока она не будет действительно исполнена. Это не красиво.
            //Но как по-другому - не знаю.
            await Task.Run(() => {
                try
                {
                    int count = 0;
                    Chat chat = bot.GetChatAsync(GameChatId).Result;
                    while (!chat.Permissions.Equals(permissions)
                        && count < 500)
                    {

                        bool r = bot.SetChatPermissionsAsync(GameChatId, permissions).IsCompletedSuccessfully;
                        chat = bot.GetChatAsync(GameChatId).Result;
                        ++count;
                    }
                }
                catch { }
            });
        }

        //Закрываем пользователя
        public async void CloseUser(long userId)
        {

            //Делаю всё в try-catch потому Телеграм может дать исключение
            //, которое для нас ничего не значит, но ломает программу
            try
            {
                await bot.RestrictChatMemberAsync(GameChatId, userId, GetClosePermission());
            }
            catch { }
        }

        //Открываем пользователя и снимаем с него все ограничения
        private void OpenUser(long userId)
        {

            try
            {
                //Полностью снять индивидуальные ограничения можно только функцией Promote, без доп разрешений
                bot.PromoteChatMemberAsync(GameChatId, userId);
            }
            catch { }
        }

        //Получаем разрешения в открытом режиме чата
        private ChatPermissions GetOpenPermissions()
        {

            ChatPermissions permissions = new ChatPermissions();
            permissions.CanInviteUsers = true;

            permissions.CanSendPolls = false;
            permissions.CanChangeInfo = false;
            permissions.CanPinMessages = false;
            permissions.CanSendMediaMessages = false;
            permissions.CanAddWebPagePreviews = false;

            permissions.CanSendMessages = true;
            permissions.CanSendOtherMessages = true;

            return permissions;
        }

        //Получаем разрешения в закрытом режиме чата
        private ChatPermissions GetClosePermission()
        {

            ChatPermissions permissions = new ChatPermissions();
            permissions.CanInviteUsers = true;
            permissions.CanSendOtherMessages = false;
            permissions.CanSendMessages = false;

            permissions.CanSendPolls = false;
            permissions.CanChangeInfo = false;
            permissions.CanPinMessages = false;
            permissions.CanSendMediaMessages = false;
            permissions.CanAddWebPagePreviews = false;

            return permissions;
        }

        //Получаем ссылку на сообщение из уникального номера
        protected string GetLinkFromMessageId(long idChat, int messageId){

            if (string.IsNullOrEmpty(GameChat.Username)){
                return $"https://t.me/c/{idChat.ToString().Substring(4)}/{messageId}";
            }else{
                return $"https://t.me/{GameChat.Username.Trim('@')}/{messageId}";
            }
        }

        #endregion

        #region SEND AND EDIT MESSAGE

        //Отправляем сообщение в игровой чат
        protected async void SendChatMessage(string text, ReplyKeyboardMarkup keyboard, bool disableNotification
            , int replayToMessageId, bool disableWebPagePrewiew){

            try {
                await bot.SendTextMessageAsync(chatId: GameChatId
                , text: text
                , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html
                , replyMarkup: keyboard
                , disableNotification: disableNotification
                , disableWebPagePreview: disableWebPagePrewiew
                , replyToMessageId: replayToMessageId);
            } catch (Exception ex) {
                WriteLog(ex.Message);
                WriteLog(ex.Source);
            }
        }

        //Отправляем сообщение в игровой чат
        protected Int32 SendChatMessageWithId(string text, ReplyKeyboardMarkup keyboard, bool disableNotification
            , int replayToMessageId, bool disableWebPagePrewiew) {

            try {
                return bot.SendTextMessageAsync(chatId: GameChatId
                    , text: text
                    , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html
                    , replyMarkup: keyboard
                    , disableNotification: disableNotification
                    , disableWebPagePreview: disableWebPagePrewiew
                    , replyToMessageId: replayToMessageId).Result.MessageId;
            } catch (Exception ex) {
                WriteLog(ex.Message);
                WriteLog(ex.Source);
                return 0;
            }
        }

        //Редактируем сообщение в канале
        protected async void EditChannelMessage(int idEditMessage, string text, bool disableWebPagePrewiew){
            await Task.Run(() => {
                try{
                    bot.EditMessageTextAsync(GameChannelId,
                       idEditMessage,
                       text: text,
                       parseMode: Telegram.Bot.Types.Enums.ParseMode.Html,
                       disableWebPagePreview: disableWebPagePrewiew);
                }catch { }
            });
        }

        //Отправляем сообщение с результатом
        protected Message SendChatMessageWithResult(string text, ReplyKeyboardMarkup keyboard, bool disableNotification
            , int replayToMessageId, bool disableWebPagePrewiew){

            return bot.SendTextMessageAsync(chatId: GameChatId
                , text: text
                , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html
                , replyMarkup: keyboard
                , disableNotification: disableNotification
                , disableWebPagePreview: disableWebPagePrewiew
                , replyToMessageId: replayToMessageId).Result;
        }

        //Отравляем сообщение в канал
        protected Message SendChannelMessage(string text, ReplyKeyboardMarkup keyboard, bool disableNotification
            , bool disableWebPagePrewiew){

            if (GameChannelId != 0){
                try{
                    return bot.SendTextMessageAsync(chatId: GameChannelId
                            , text: text
                            , parseMode: Telegram.Bot.Types.Enums.ParseMode.Html
                            , replyMarkup: keyboard
                            , disableNotification: disableNotification
                            , disableWebPagePreview: disableWebPagePrewiew).Result;
                }catch{
                    return null;
                }
            }
            return null;
        }

        //Удаляем сообщение из чата
        private async void DeleteMessage(int idMessage){
            await bot.DeleteMessageAsync(chatId: GameChatId,
                messageId: idMessage);
        }

        #endregion

        #region DATA BASE

        //Создаём таблицу действующих игроков
        private void CreateTableActivePlayers(SQLiteConnection cnct){
            lock (dbLocker)
            {
                OpenDatabase();
                string sCommand = @"CREATE TABLE IF NOT EXISTS ActivePlayers (
                    Id    INTEGER NOT NULL UNIQUE,
                    FirstName TEXT NOT NULL,
	                LastName  TEXT,
                    Score  INTEGER NOT NULL DEFAULT 0,
	                WeekScore INTEGER NOT NULL DEFAULT 0,
	                MonthScore    INTEGER NOT NULL DEFAULT 0,
                    ChancesLeft   INTEGER NOT NULL DEFAULT 0,
	                WeekChances    INTEGER NOT NULL DEFAULT 0,
	                MonthChances   INTEGER NOT NULL DEFAULT 0,
                    LastMessageId INTEGER NOT NULL DEFAULT 0,
	                DateLastMessage   TEXT,
	                PRIMARY KEY(Id)); ";

                SQLiteCommand command = new SQLiteCommand();

                command.Connection = cnct;
                command.CommandText = sCommand;
                command.ExecuteNonQuery();

                CloseDatabase();
            }
        }


        //Записываем результаты в таблицу
        protected void SaveActivePlayerResults(ActivePlayer activePlayer)
        {

            lock (dbLocker)
            {
                OpenDatabase();
                string sCommand = "SELECT * FROM ActivePlayers WHERE Id =@id";

                SQLiteCommand command = new SQLiteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@id", activePlayer.Id);

                dataReader = command.ExecuteReader();
                if (dataReader != null && (dataReader.HasRows))
                {

                    sCommand = "UPDATE ActivePlayers SET FirstName =@firstName, LastName =@lastName, Score =@score," +
                        "WeekScore =@weekScore, MonthScore =@monthScore," +
                        "ChancesLeft =@chancesLeft, WeekChances =@weekChances," +
                        "MonthChances =@monthChances, LastMessageId =@lastMessageId" +
                        ", DateLastMessage =@dateLastMessage WHERE Id =@id";

                }
                else
                {
                    sCommand = "INSERT INTO ActivePlayers (" +
                    "Id, FirstName, LastName, Score, WeekScore, MonthScore, ChancesLeft," +
                    "WeekChances, MonthChances, LastMessageId, DateLastMessage) VALUES" +
                    " (@id, @firstName, @lastName, @score, @weekScore, @monthScore, @chancesLeft," +
                    "@weekChances, @monthChances, @lastMessageId, @dateLastMessage)";
                }

                dataReader.Close();
                command.CommandText = sCommand;

                command.Parameters.AddWithValue("@id", activePlayer.Id);
                command.Parameters.AddWithValue("@firstName", activePlayer.FirstName);
                command.Parameters.AddWithValue("@lastName", activePlayer.LastName);
                command.Parameters.AddWithValue("@score", activePlayer.Score);
                command.Parameters.AddWithValue("@weekScore", activePlayer.WeekScore);
                command.Parameters.AddWithValue("@monthScore", activePlayer.MonthScore);
                command.Parameters.AddWithValue("@chancesLeft", activePlayer.ChancesLeft);
                command.Parameters.AddWithValue("@weekChances", activePlayer.WeekChances);
                command.Parameters.AddWithValue("@monthChances", activePlayer.MonthChances);

                command.Parameters.AddWithValue("@lastMessageId", activePlayer.LastMessageId);
                command.Parameters.AddWithValue("@dateLastMessage", activePlayer.DateLastMessage.ToString());

                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }

        //Получаем из базы данных обновлённые сведения о действующем игроке
        protected ActivePlayer LoadActivePlayer(long idUser)
        {

            lock (dbLocker)
            {
                OpenDatabase();
                string sCommand = "SELECT * FROM ActivePlayers WHERE Id =@id";

                SQLiteCommand command = new SQLiteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;
                command.Parameters.AddWithValue("@id", idUser);

                ActivePlayer activePlayer = null;
                dataReader = command.ExecuteReader();
                if (dataReader != null && (dataReader.HasRows))
                {
                    while (dataReader.Read())
                    {
                        User user = new User();

                        user.Id = idUser;
                        user.FirstName = dataReader["FirstName"].ToString();
                        user.LastName = dataReader["LastName"].ToString();

                        activePlayer = new ActivePlayer(user, 0);
                        activePlayer.Score = int.Parse(dataReader["Score"].ToString());
                        activePlayer.WeekScore = int.Parse(dataReader["WeekScore"].ToString());
                        activePlayer.MonthScore = int.Parse(dataReader["MonthScore"].ToString());
                        activePlayer.ChancesLeft = int.Parse(dataReader["ChancesLeft"].ToString());
                        activePlayer.WeekChances = int.Parse(dataReader["WeekChances"].ToString());
                        activePlayer.MonthChances = int.Parse(dataReader["MonthChances"].ToString());

                        activePlayer.LastMessageId = int.Parse(dataReader["LastMessageId"].ToString());
                        activePlayer.DateLastMessage = DateTime.Parse(dataReader["DateLastMessage"].ToString());

                    }
                }
                dataReader.Close();
                CloseDatabase();
                return activePlayer;
            }
        }

        //Получаем результаты действующих пользователей
        protected List<ActivePlayer> LoadActivePlayersResults(string nameScore, int startChances)
        {

            lock (dbLocker)
            {
                OpenDatabase();

                string sCommand = $"SELECT * FROM ActivePlayers WHERE ({nameScore} >0) OR (ChancesLeft !={startChances})";

                SQLiteCommand command = new SQLiteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;

                dataReader = command.ExecuteReader();
                List<ActivePlayer> results = null;
                if (dataReader != null && (dataReader.HasRows))
                {

                    results = new List<ActivePlayer>();
                    while (dataReader.Read())
                    {
                        User user = new User();

                        user.Id = long.Parse(dataReader["Id"].ToString());
                        user.FirstName = dataReader["FirstName"].ToString();
                        user.LastName = dataReader["LastName"].ToString();


                        ActivePlayer player = new ActivePlayer(user, 0);
                        player.Score = int.Parse(dataReader["Score"].ToString());
                        player.WeekScore = int.Parse(dataReader["WeekScore"].ToString());
                        player.MonthScore = int.Parse(dataReader["MonthScore"].ToString());
                        player.ChancesLeft = int.Parse(dataReader["ChancesLeft"].ToString());
                        player.WeekChances = int.Parse(dataReader["WeekChances"].ToString());
                        player.MonthChances = int.Parse(dataReader["MonthChances"].ToString());

                        player.LastMessageId = int.Parse(dataReader["LastMessageId"].ToString());
                        player.DateLastMessage = DateTime.Parse(dataReader["DateLastMessage"].ToString());

                        results.Add(player);
                    }
                }
                dataReader.Close();
                CloseDatabase();
                return results;
            }
        }


        //Сбрасываем текущие результаты действующих игроков
        protected void ResetActivePlayersResults(int chancesLeft)
        {

            lock (dbLocker)
            {
                OpenDatabase();
                string sCommand = $"UPDATE ActivePlayers SET Score =0, ChancesLeft =@chancesLeft";

                SQLiteCommand command = new SQLiteCommand();
                command.CommandText = sCommand;
                command.Connection = connection;

                command.Parameters.AddWithValue("@chancesLeft", chancesLeft);
                command.ExecuteNonQuery();

                command.CommandText = sCommand;
                command.ExecuteNonQuery();
                CloseDatabase();
            }
        }
        #endregion
    }
}


