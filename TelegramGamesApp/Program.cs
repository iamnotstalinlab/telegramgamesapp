﻿//Основной модуль с которого запускается программа
//

using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace TelegramGamesApp
{
    class Program
    {

        //Запускаем программу
        static void Main(string[] args)
        {

            Program program = new Program();
            Console.WriteLine("Здравствуйте, я готов потрудиться! / Hello, I am ready to work." +
                "\nВведите токен телеграм-бота: / Enter telegram-bot's token:");

            //string token = "5622788096:AAGCD3zXKUrmWZhCT4lJaK-MaRFTGljfhv4";
            string token = Console.ReadLine();
            try {
                program.LaunchBot(token);
            }catch (Exception ex){

                program.WriteLog("Дата и время ошибки: " + DateTime.Now.ToString("dd/MM/yyyy HH:mm"));
                program.WriteLog(ex.GetType().Name);
                program.WriteLog(ex.Message);
                program.WriteLog(ex.StackTrace);
                program.WriteLog(ex.Source);
                program.WriteLog(ex.Data.ToString());
                program.WriteLog(ex.HResult.ToString());
            }

            Console.WriteLine("Принял. / I got it." +
                "\nТеперь я могу общаться с вами в Телеграм. / I am ready to talk with us in the Telegram, now." +
                "\n\nНапишите в личные сообщения, там мы всё настроим и будем оттуда управлять игрой. Вам так будет удобнее." +
                "\n/ Write to me in Telegram. We will set and manage game from there. It will be comfortable for you." +
                "\n\nВведите 'exit', чтобы завершить программу / Enter 'exit' for finished the program.");

            string command = string.Empty;
            while (command != "exit")
            {
                command = Console.ReadLine();
            }
        }

        //Запускаем бота
        public void LaunchBot(string token)
        {
            TelegramBot bot = new TelegramBot(token);

        }

        //Делаем записи в журнал
        public void WriteLog(string text)
        {

            using (FileStream fstream = new FileStream("./Logs", FileMode.OpenOrCreate))
            {
                byte[] buffer = Encoding.Default.GetBytes("\n" + text);    // преобразуем строку в байты
                fstream.Seek(0, SeekOrigin.End);
                fstream.Write(buffer, 0, buffer.Length); // запись массива байтов в файл
            }
        }

    }
}
